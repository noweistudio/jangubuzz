import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAutoCompleteComponent } from './user-auto-complete.component';

describe('UserAutoCompleteComponent', () => {
  let component: UserAutoCompleteComponent;
  let fixture: ComponentFixture<UserAutoCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAutoCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
