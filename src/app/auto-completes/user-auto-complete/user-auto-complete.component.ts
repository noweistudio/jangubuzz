import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AutoCompleteComponent} from '../auto-complete.component';
import {debounceTime} from 'rxjs/internal/operators';
import {UserService} from '../../services/user.service';

@Component({
    selector: 'app-user-auto-complete',
    templateUrl: './user-auto-complete.component.html',
    styleUrls: ['./user-auto-complete.component.scss']
})
export class UserAutoCompleteComponent extends AutoCompleteComponent {
    @Input() placeholder: string = 'Find User';

    @Output() onSelect = new EventEmitter();

    constructor(public userService: UserService) {
        super();

        this.options = this.userService.index(this.formControl.value);

        this.formControl.valueChanges.pipe(
            debounceTime(this.debounceTime)
        ).subscribe(() => {
            this.options = this.userService.index(this.formControl.value);
        });
    }

    public select(): void {
        this.onSelect.emit(this.formControl.value);
        this.formControl.patchValue('');
    }
}