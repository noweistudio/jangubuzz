import {Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/index';

const DEBOUNCE_TIME: number = 1000;

export abstract class AutoCompleteComponent {
    @Input() placeholder: string;

    public formControl: FormControl = new FormControl('');
    public options: Observable<any>;

    protected debounceTime: number = DEBOUNCE_TIME;

    constructor() {
    }
}