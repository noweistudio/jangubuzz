import {ChangeDetectorRef, Component, Input, SimpleChanges} from '@angular/core';
import {ListItemComponent} from '../list-item.component';
import {Observable} from 'rxjs';
import {UserService} from '../../services/user.service';
import {ListingCommentReplyService} from '../../services/listing-comment-reply.service';
import {StandardBottomSheetComponent} from '../../bottom-sheets/standard-bottom-sheet/standard-bottom-sheet.component';
import {MatBottomSheet} from '@angular/material';
import {ListingCommentService} from '../../services/listing-comment.service';

@Component({
    selector: 'app-comment-list-item',
    templateUrl: './comment-list-item.component.html',
    styleUrls: ['./comment-list-item.component.scss']
})
export class CommentListItemComponent extends ListItemComponent {
    @Input() commentId: string = '';
    @Input() eventId: string = '';
    @Input() userId: string = '';
    @Input() hostId: string = '';

    public commenterObservable: Observable<any>;
    public commentRepliesObservable: Observable<any>;

    private _zoomIn: boolean = false;
    private _reply: boolean = false;

    constructor(public cdRef: ChangeDetectorRef,
                public bottomSheet: MatBottomSheet,
                public userService: UserService,
                public listingCommentService: ListingCommentService,
                public listingCommentReplyService: ListingCommentReplyService) {
        super();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.userId) {
            this.commenterObservable = this.userService.read(this.userId);
        }
        if (changes.model && this.model.listingId && this.commentId && !this.commentRepliesObservable) {
            this.commentRepliesObservable = this.listingCommentReplyService.index(this.model.listingId, this.commentId);
        }
    }

    get zoomIn(): boolean {
        return this._zoomIn;
    }

    set zoomIn(bool: boolean) {
        this._zoomIn = bool;
    }

    get reply(): boolean {
        return this._reply;
    }

    set reply(bool: boolean) {
        this._reply = bool;
        this.cdRef.detectChanges();
    }

    public openCommentActionBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(StandardBottomSheetComponent, {});

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'delete':
                    this.listingCommentService.delete(this.eventId, this.commentId);
                    break;
                default:
            }
        });
    }
}