import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplyFormListItemComponent } from './reply-form-list-item.component';

describe('ReplyFormListItemComponent', () => {
  let component: ReplyFormListItemComponent;
  let fixture: ComponentFixture<ReplyFormListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplyFormListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyFormListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
