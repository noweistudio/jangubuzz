import {Component, Input} from '@angular/core';
import {ListItemComponent} from '../list-item.component';
import {AuthService} from '../../services/auth.service';
import {ListingCommentReplyService} from '../../services/listing-comment-reply.service';

@Component({
    selector: 'app-reply-form-list-item',
    templateUrl: './reply-form-list-item.component.html',
    styleUrls: ['./reply-form-list-item.component.scss']
})
export class ReplyFormListItemComponent extends ListItemComponent {
    @Input() commentId: string = '';
    @Input() eventId: string = '';
    @Input() hostId: string = '';
    @Input() commentHostId: string = '';

    constructor(public authService: AuthService,
                public listingCommentReplyService: ListingCommentReplyService) {
        super();
    }

    public create(): void {
        let payload: object = {
            reply: this.model.reply,
            userId: this.authService.user.uid,
            hostId: this.hostId,
            commentId: this.commentId,
            listingId: this.eventId,
            dateCreated: new Date().toISOString(),
            dateModified: new Date().toISOString(),
            commentHostId: this.commentHostId
        };

        this.listingCommentReplyService.create(this.eventId, this.commentId, payload);
        this.model = {};
    }
}