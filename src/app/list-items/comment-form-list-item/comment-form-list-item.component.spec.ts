import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentFormListItemComponent } from './comment-form-list-item.component';

describe('CommentFormListItemComponent', () => {
  let component: CommentFormListItemComponent;
  let fixture: ComponentFixture<CommentFormListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentFormListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentFormListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
