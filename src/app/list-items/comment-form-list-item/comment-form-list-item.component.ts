import {Component, Input} from '@angular/core';
import {ListItemComponent} from '../list-item.component';
import {AuthService} from '../../services/auth.service';
import {ListingCommentService} from '../../services/listing-comment.service';

@Component({
    selector: 'app-comment-form-list-item',
    templateUrl: './comment-form-list-item.component.html',
    styleUrls: ['./comment-form-list-item.component.scss']
})
export class CommentFormListItemComponent extends ListItemComponent {
    @Input() eventId: string = '';
    @Input() hostId: string = '';

    constructor(public authService: AuthService,
                public listingCommentService: ListingCommentService) {
        super();
    }

    public create(): void {
        let payload: object = {
            userId: this.authService.user.uid,
            type: 'text',
            hostId: this.hostId,
            listingId: this.eventId,
            comment: this.model.comment,
            dateCreated: new Date().toISOString(),
            dateModified: new Date().toISOString(),
            commentImage: ''
        };

        this.listingCommentService.create(this.eventId, payload);
        this.model = {};
    }
}