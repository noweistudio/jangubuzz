import {EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export abstract class ListItemComponent implements OnChanges {
    @Input() model: any = {};
    @Output() modelChange = new EventEmitter();

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges) {
    }
}