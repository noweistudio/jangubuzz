import {Component, Input, SimpleChanges} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../../services/user.service';
import {ListItemComponent} from '../list-item.component';
import {StandardBottomSheetComponent} from '../../bottom-sheets/standard-bottom-sheet/standard-bottom-sheet.component';
import {ListingCommentReplyService} from '../../services/listing-comment-reply.service';
import {MatBottomSheet} from '@angular/material';

@Component({
    selector: 'app-reply-list-item',
    templateUrl: './reply-list-item.component.html',
    styleUrls: ['./reply-list-item.component.scss']
})
export class ReplyListItemComponent extends ListItemComponent {
    @Input() replyId: string = '';
    @Input() commentId: string = '';
    @Input() eventId: string = '';
    @Input() userId: string = '';

    public replyerObservable: Observable<any>;

    constructor(public bottomSheet: MatBottomSheet,
                public userService: UserService,
                public listingCommentReplyService: ListingCommentReplyService,) {
        super();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.userId) {
            this.replyerObservable = this.userService.read(this.userId);
        }
    }

    public openReplyActionBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(StandardBottomSheetComponent, {});

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'delete':
                    this.listingCommentReplyService.delete(this.eventId, this.commentId, this.replyId);
                    break;
                default:
            }
        });
    }
}