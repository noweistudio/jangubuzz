import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplyListItemComponent } from './reply-list-item.component';

describe('ReplyListItemComponent', () => {
  let component: ReplyListItemComponent;
  let fixture: ComponentFixture<ReplyListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplyListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
