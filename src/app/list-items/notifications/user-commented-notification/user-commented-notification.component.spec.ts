import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCommentedNotificationComponent } from './user-commented-notification.component';

describe('UserCommentedNotificationComponent', () => {
  let component: UserCommentedNotificationComponent;
  let fixture: ComponentFixture<UserCommentedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCommentedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCommentedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
