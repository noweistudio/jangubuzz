import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-user-commented-notification',
    templateUrl: './user-commented-notification.component.html',
    styleUrls: ['./user-commented-notification.component.scss']
})
export class UserCommentedNotificationComponent extends NotificationComponent {
}