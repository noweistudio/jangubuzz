import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventWasCanceledNotificationComponent } from './event-was-canceled-notification.component';

describe('EventWasCanceledNotificationComponent', () => {
  let component: EventWasCanceledNotificationComponent;
  let fixture: ComponentFixture<EventWasCanceledNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventWasCanceledNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventWasCanceledNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
