import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
  selector: 'app-event-was-canceled-notification',
  templateUrl: './event-was-canceled-notification.component.html',
  styleUrls: ['./event-was-canceled-notification.component.scss']
})
export class EventWasCanceledNotificationComponent extends NotificationComponent {
}