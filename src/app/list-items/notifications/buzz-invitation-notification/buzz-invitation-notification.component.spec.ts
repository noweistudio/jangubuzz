import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzzInvitationNotificationComponent } from './buzz-invitation-notification.component';

describe('BuzzInvitationNotificationComponent', () => {
  let component: BuzzInvitationNotificationComponent;
  let fixture: ComponentFixture<BuzzInvitationNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzzInvitationNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzzInvitationNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
