import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-buzz-invitation-notification',
    templateUrl: './buzz-invitation-notification.component.html',
    styleUrls: ['./buzz-invitation-notification.component.scss']
})
export class BuzzInvitationNotificationComponent extends NotificationComponent {
}