import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowingNotificationComponent } from './following-notification.component';

describe('FollowingNotificationComponent', () => {
  let component: FollowingNotificationComponent;
  let fixture: ComponentFixture<FollowingNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowingNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowingNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
