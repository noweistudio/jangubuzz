import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-following-notification',
    templateUrl: './following-notification.component.html',
    styleUrls: ['./following-notification.component.scss']
})
export class FollowingNotificationComponent extends NotificationComponent {
}