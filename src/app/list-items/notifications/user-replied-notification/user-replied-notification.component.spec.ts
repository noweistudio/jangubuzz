import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRepliedNotificationComponent } from './user-replied-notification.component';

describe('UserRepliedNotificationComponent', () => {
  let component: UserRepliedNotificationComponent;
  let fixture: ComponentFixture<UserRepliedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRepliedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRepliedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
