import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-user-replied-notification',
    templateUrl: './user-replied-notification.component.html',
    styleUrls: ['./user-replied-notification.component.scss']
})
export class UserRepliedNotificationComponent extends NotificationComponent {
}