import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserJoinedEventNotificationComponent } from './user-joined-event-notification.component';

describe('UserJoinedEventNotificationComponent', () => {
  let component: UserJoinedEventNotificationComponent;
  let fixture: ComponentFixture<UserJoinedEventNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserJoinedEventNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserJoinedEventNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
