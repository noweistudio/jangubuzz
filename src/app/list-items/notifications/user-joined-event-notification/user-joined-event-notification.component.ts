import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
  selector: 'app-user-joined-event-notification',
  templateUrl: './user-joined-event-notification.component.html',
  styleUrls: ['./user-joined-event-notification.component.scss']
})
export class UserJoinedEventNotificationComponent extends NotificationComponent {
}