import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-refund-granted-notification',
    templateUrl: './refund-granted-notification.component.html',
    styleUrls: ['./refund-granted-notification.component.scss']
})
export class RefundGrantedNotificationComponent extends NotificationComponent {
}