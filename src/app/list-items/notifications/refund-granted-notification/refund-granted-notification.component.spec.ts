import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundGrantedNotificationComponent } from './refund-granted-notification.component';

describe('RefundGrantedNotificationComponent', () => {
  let component: RefundGrantedNotificationComponent;
  let fixture: ComponentFixture<RefundGrantedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundGrantedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundGrantedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
