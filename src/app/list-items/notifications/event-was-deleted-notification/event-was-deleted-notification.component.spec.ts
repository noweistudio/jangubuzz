import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventWasDeletedNotificationComponent } from './event-was-deleted-notification.component';

describe('EventWasDeletedNotificationComponent', () => {
  let component: EventWasDeletedNotificationComponent;
  let fixture: ComponentFixture<EventWasDeletedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventWasDeletedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventWasDeletedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
