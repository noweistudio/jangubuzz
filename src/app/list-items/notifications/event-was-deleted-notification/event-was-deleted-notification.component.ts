import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
  selector: 'app-event-was-deleted-notification',
  templateUrl: './event-was-deleted-notification.component.html',
  styleUrls: ['./event-was-deleted-notification.component.scss']
})
export class EventWasDeletedNotificationComponent extends NotificationComponent {
}