import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEditNotificationComponent } from './team-edit-notification.component';

describe('TeamEditNotificationComponent', () => {
  let component: TeamEditNotificationComponent;
  let fixture: ComponentFixture<TeamEditNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamEditNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEditNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
