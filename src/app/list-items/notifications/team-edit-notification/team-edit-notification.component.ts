import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-team-edit-notification',
    templateUrl: './team-edit-notification.component.html',
    styleUrls: ['./team-edit-notification.component.scss']
})
export class TeamEditNotificationComponent extends NotificationComponent {
}