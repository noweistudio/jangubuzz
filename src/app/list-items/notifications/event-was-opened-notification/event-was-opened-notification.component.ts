import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-event-was-opened-notification',
    templateUrl: './event-was-opened-notification.component.html',
    styleUrls: ['./event-was-opened-notification.component.scss']
})
export class EventWasOpenedNotificationComponent extends NotificationComponent {
}