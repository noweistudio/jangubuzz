import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventWasOpenedNotificationComponent } from './event-was-opened-notification.component';

describe('EventWasOpenedNotificationComponent', () => {
  let component: EventWasOpenedNotificationComponent;
  let fixture: ComponentFixture<EventWasOpenedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventWasOpenedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventWasOpenedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
