import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundRequestedNotificationComponent } from './refund-requested-notification.component';

describe('RefundRequestedNotificationComponent', () => {
  let component: RefundRequestedNotificationComponent;
  let fixture: ComponentFixture<RefundRequestedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundRequestedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundRequestedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
