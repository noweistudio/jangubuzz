import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-refund-requested-notification',
    templateUrl: './refund-requested-notification.component.html',
    styleUrls: ['./refund-requested-notification.component.scss']
})
export class RefundRequestedNotificationComponent extends NotificationComponent {
}