import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundDeclinedNotificationComponent } from './refund-declined-notification.component';

describe('RefundDeclinedNotificationComponent', () => {
  let component: RefundDeclinedNotificationComponent;
  let fixture: ComponentFixture<RefundDeclinedNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundDeclinedNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundDeclinedNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
