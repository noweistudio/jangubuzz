import {Component} from '@angular/core';
import {NotificationComponent} from '../notification.component';

@Component({
    selector: 'app-refund-declined-notification',
    templateUrl: './refund-declined-notification.component.html',
    styleUrls: ['./refund-declined-notification.component.scss']
})
export class RefundDeclinedNotificationComponent extends NotificationComponent {
}