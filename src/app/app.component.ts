import {Component} from "@angular/core";
import {AuthService} from "./services/auth.service";
import {LoadingService} from "./services/loading.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(public authService: AuthService,
                public loadingService: LoadingService) {
    }
}