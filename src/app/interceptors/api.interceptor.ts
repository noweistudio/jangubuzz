import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {AngularFireAuth} from 'angularfire2/auth';
import {mergeMap} from 'rxjs/internal/operators';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    constructor(public afAuth: AngularFireAuth,
    ) {
    }

    /**
     * Intercept out going api request and set header
     * @param {HttpRequest<any>} req
     * @param {HttpHandler} next
     * @returns {Observable<HttpEvent<any>>}
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.afAuth.auth.currentUser.getIdToken()).pipe(mergeMap((token: string) => {
            const authReq = req.clone({
                headers: new HttpHeaders({
                    // todo here
                    //'Content-Type':  'application/json',
                    //'Authorization': token,
                    authorization: 'Bearer ' + token
                })
            });

            console.log(authReq);
            return next.handle(authReq);
        }));
    }
}