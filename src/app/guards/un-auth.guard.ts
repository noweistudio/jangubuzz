import {Injectable} from '@angular/core';
import {Router, CanActivate, CanActivateChild} from '@angular/router';
import {AuthService} from "../services/auth.service";

@Injectable()
export class UnAuthGuard implements CanActivate, CanActivateChild {
    constructor(public router: Router,
                public authService: AuthService) {
    }

    /**
     * Can active route if user is not authenticated
     * @returns {Promise<boolean>}
     */
    canActivate(): Promise<boolean> {
        return new Promise((resolve) => {
            this.authService.loadUser().then(() => {
                if (!this.authService.user) {
                    resolve(true);
                    return;
                }

                this.router.navigateByUrl('page');
                resolve(true);
            }).catch(() => {
                resolve(true);
            });
        });
    }

    /**
     * Can active child route if user is not authenticated
     * @returns {Promise<boolean>}
     */
    canActivateChild(): Promise<boolean> {
        return new Promise((resolve) => {
            this.authService.loadUser().then(() => {
                if (!this.authService.user) {
                    resolve(true);
                    return;
                }

                this.router.navigateByUrl('page');
                resolve(true);
            }).catch(() => {
                resolve(true);
            });
        });
    }
}