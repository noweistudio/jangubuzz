import {Injectable} from "@angular/core";
import {CanActivate, CanActivateChild, Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(public router: Router,
                public authService: AuthService) {
    }

    /**
     * Can active route if user is authenticated
     * @returns {Promise<boolean>}
     */
    canActivate(): Promise<boolean> {
        return new Promise((resolve) => {
            this.authService.loadUser().then(() => {
                if (this.authService.user) {
                    resolve(true);
                    return;
                }

                this.router.navigateByUrl('login');
                resolve(false);
            }).catch(() => {
                resolve(false);
            });
        });
    }

    /**
     * Can active child route if user is authenticated
     * @returns {Promise<boolean>}
     */
    canActivateChild(): Promise<boolean> {
        return new Promise((resolve) => {
            this.authService.loadUser().then(() => {
                if (this.authService.user) {
                    resolve(true);
                    return;
                }

                this.router.navigateByUrl('login');
                resolve(false);
            }).catch(() => {
                resolve(false);
            });
        });
    }
}