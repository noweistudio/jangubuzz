import {EventEmitter, Input, Output} from '@angular/core';

export abstract class CardComponent {
    @Input() model: any = {};
    @Output() modelChange = new EventEmitter();

    constructor() {
    }
}