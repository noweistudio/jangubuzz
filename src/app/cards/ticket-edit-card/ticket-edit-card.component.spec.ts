import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketEditCardComponent } from './ticket-edit-card.component';

describe('TicketEditCardComponent', () => {
  let component: TicketEditCardComponent;
  let fixture: ComponentFixture<TicketEditCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketEditCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketEditCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
