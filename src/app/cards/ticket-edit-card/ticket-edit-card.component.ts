import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CardComponent} from '../cards.components';

@Component({
    selector: 'app-ticket-edit-card',
    templateUrl: './ticket-edit-card.component.html',
    styleUrls: ['./ticket-edit-card.component.scss']
})
export class TicketEditCardComponent extends CardComponent {
    @Input() listing: any;
    @Output() onEdit = new EventEmitter();
    @Output() onDelete = new EventEmitter();

    constructor() {
        super();
    }
}