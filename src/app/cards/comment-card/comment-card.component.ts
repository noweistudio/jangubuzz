import {Component, Input} from '@angular/core';
import {CardComponent} from '../cards.components';

@Component({
    selector: 'app-comment-card',
    templateUrl: './comment-card.component.html',
    styleUrls: ['./comment-card.component.scss']
})
export class CommentCardComponent extends CardComponent {
    @Input() eventId: string = '';
    @Input() hostId: string = '';
}