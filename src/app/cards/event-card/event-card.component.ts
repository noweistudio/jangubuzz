import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {CardComponent} from '../cards.components';
import {UserListingBookmarkedService} from '../../services/user-listing-bookmarked.service';
import {Observable} from 'rxjs/index';
import {ListingService} from '../../services/listing.service';
import {ListingBookmarkService} from '../../services/listing-bookmark.service';
import {HelperService} from '../../services/helper.service';
import {AuthService} from '../../services/auth.service';
import {EventBottomSheetComponent} from '../../bottom-sheets/event-bottom-sheet/event-bottom-sheet.component';
import {Router} from '@angular/router';
import {MatBottomSheet, MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';

@Component({
    selector: 'app-event-card',
    templateUrl: './event-card.component.html',
    styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent extends CardComponent implements OnChanges {
    @Input() id: string = '';

    public currentUserIsBookmarkedObservable: Observable<any>;

    constructor(public listingService: ListingService,
                public listingBookmarkService: ListingBookmarkService,
                public userListingBookmarkedService: UserListingBookmarkedService,
                public authService: AuthService,
                protected router: Router,
                protected bottomSheet: MatBottomSheet,
                protected dialog: MatDialog) {
        super();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.id) {
            this.currentUserIsBookmarkedObservable = this.userListingBookmarkedService.currentUserIsBookmarked(this.id);
        }
    }

    get isHostedByCurrentUser(): boolean {
        if (!this.model || !this.authService.user) {
            return false;
        }
        return this.model.userId === this.authService.user.uid;
    }

    get hitLevel(): number {
        let weights: number = this.model.viewsCount * HelperService.getHeatLevelWeights().VIEW +
            this.model.bookmarksCount * HelperService.getHeatLevelWeights().BOOKMARK +
            this.model.commentsCount * HelperService.getHeatLevelWeights().COMMENT +
            this.model.repliesCount * HelperService.getHeatLevelWeights().REPLY +
            this.model.goingCount * HelperService.getHeatLevelWeights().GOING +
            this.model.ticketsCount * HelperService.getHeatLevelWeights().TICKET;

        let days: number = (HelperService.getNow().getTime() - this.model.dateCreated.seconds * 1000) / HelperService.getDayUnits().MILLISECONDS;

        let hitLevel: number = Math.round(weights / days);
        return hitLevel < HelperService.getHeatLevelThresholds().MAX ? hitLevel : HelperService.getHeatLevelThresholds().MAX;
    }

    get hitLevelColor(): string {
        if (this.hitLevel > HelperService.getHeatLevelThresholds().III) {
            return HelperService.getHeatLevelColors().III;
        } else if (this.hitLevel > HelperService.getHeatLevelThresholds().II) {
            return HelperService.getHeatLevelColors().II;
        } else if (this.hitLevel > HelperService.getHeatLevelThresholds().I) {
            return HelperService.getHeatLevelColors().I;
        }
        return HelperService.getHeatLevelColors().ZERO;
    }

    public bookmark(): void {
        Promise.all([
            this.listingBookmarkService.bookmarkAsCurrentUser(this.id),
            this.userListingBookmarkedService.bookmarkAsCurrentUser(this.id, this.model)
        ]);
    }

    public unbookmark(): void {
        Promise.all([
            this.listingBookmarkService.unbookmarkAsCurrentUser(this.id),
            this.userListingBookmarkedService.unbookmarkAsCurrentUser(this.id)
        ]);
    }

    public openEventBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(EventBottomSheetComponent, {
            data: {
                eventId: this.id,
                disabled: {
                    edit: !this.isHostedByCurrentUser,
                    viewTicketTimeline: !this.isHostedByCurrentUser,
                    delete: !!this.model.ticketsCount
                }
            }
        });

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'invite':
                    this.router.navigateByUrl('/page/buzz-invite/' + this.id);
                    break;
                case 'edit':
                    this.router.navigateByUrl('/page/event-edit/' + this.id);
                    break;
                case 'viewTicketTimeline':
                    this.router.navigateByUrl('/page/ticket-timeline/' + this.id);
                    break;
                case 'delete':
                    this.openDeleteEventConfirmationDialog();
                    break;
                default:
            }
        });
    }

    public openDeleteEventConfirmationDialog(): void {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '300px',
            maxWidth: '80%',
            data: {
                dialogTitle: 'Delete Event Confirmation',
                model: {
                    confirmation: 'Are you sure you want to delete this event?'
                }
            }
        }).afterClosed().subscribe((res) => {
            switch (res) {
                case 'confirm':
                    this.listingService.delete(this.id).then(() => {
                        this.router.navigateByUrl('/page/events');
                    });
                    break;
                default:
            }
        });
    }

    public isEnded(date: any): boolean {
        return new Date(date) < new Date();
    }
}