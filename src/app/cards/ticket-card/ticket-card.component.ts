import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {CardComponent} from '../cards.components';
import {Observable} from 'rxjs';
import {MatBottomSheet, MatDialog} from '@angular/material';
import {ListingPurchasedTicketService} from '../../services/listing-purchased-ticket.service';
import {ListingService} from '../../services/listing.service';
import {PaymentService} from '../../services/payment.service';
import {QrCodeDialogComponent} from '../../dialogs/qr-code-dialog/qr-code-dialog.component';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import {TicketBottomSheetComponent} from '../../bottom-sheets/ticket-bottom-sheet/ticket-bottom-sheet.component';
import {HelperService} from '../../services/helper.service';

@Component({
    selector: 'app-ticket-card',
    templateUrl: './ticket-card.component.html',
    styleUrls: ['./ticket-card.component.scss']
})
export class TicketCardComponent extends CardComponent implements OnChanges {
    @Input() ticketPurchaseId: string = '';

    public listingPurchasedTicket: any;
    public listingObservable: Observable<any>;
    public listingPurchasedTicketObservable: Observable<any>;

    constructor(public dialog: MatDialog,
                public bottomSheet: MatBottomSheet,
                public listingService: ListingService,
                public listingPurchasedTicketService: ListingPurchasedTicketService,
                public paymentService: PaymentService) {
        super();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.model && this.model.listingId && !this.listingObservable) {
            this.listingObservable = this.listingService.read(this.model.listingId);
        }

        if (changes.ticketPurchaseId && !this.listingPurchasedTicketObservable) {
            this.listingPurchasedTicketObservable = this.listingPurchasedTicketService.read(this.model.listingId, this.ticketPurchaseId);
            this.listingPurchasedTicketObservable.subscribe((res) => {
                this.listingPurchasedTicket = res;
            });
        }
    }

    public openQRCodeDialog(): void {
        this.dialog.open(QrCodeDialogComponent, {
            width: '250px',
            data: {
                dialogTitle: this.model.ticketInfo.title,
                model: {
                    code: this.ticketPurchaseId
                }
            }
        });
    }

    public openRefundTicketConfirmationDialog(): void {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '300px',
            maxWidth: '80%',
            data: {
                dialogTitle: 'Refund Ticket Confirmation',
                model: {
                    confirmation: 'Are you sure you want to request a refund?'
                }
            }
        }).afterClosed().subscribe((res) => {
            switch (res) {
                case 'confirm':
                    let payload = HelperService.getDefaultTicketRefundRequest();
                    payload.ticketId = this.model.ticketInfo.id;
                    payload.listingId = this.model.listingId;

                    this.paymentService.requestTicketRefund(payload).subscribe();
                    break;
                default:
            }
        });
    }

    public openTicketBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(TicketBottomSheetComponent, {
            data: {
                ticketPurchaseId: this.ticketPurchaseId,
                disabled: {
                    requestRefund: !this.refundable
                }
            }
        });

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'requestRefund':
                    this.openRefundTicketConfirmationDialog();
                    break;
                default:
            }
        });
    }

    get refundable(): boolean {
        return (this.model && this.model.ticketInfo.refundable) && (this.listingPurchasedTicket && this.listingPurchasedTicket.amount === this.model.totalTickets);

    }
}