import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestsCardComponent } from './guests-card.component';

describe('GuestsCardComponent', () => {
  let component: GuestsCardComponent;
  let fixture: ComponentFixture<GuestsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
