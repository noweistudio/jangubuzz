import {Component} from '@angular/core';
import {CardComponent} from '../cards.components';

@Component({
    selector: 'app-guests-card',
    templateUrl: './guests-card.component.html',
    styleUrls: ['./guests-card.component.scss']
})
export class GuestsCardComponent extends CardComponent {
}