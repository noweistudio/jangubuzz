import {Component, ViewChild} from '@angular/core';
import {FormComponent} from '../form.component';
import {Form, NgForm} from '@angular/forms';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent extends FormComponent {
    @ViewChild(NgForm) form;
}