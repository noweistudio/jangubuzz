import {Component, Input} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-buzz-invite-form',
    templateUrl: './buzz-invite-form.component.html',
    styleUrls: ['./buzz-invite-form.component.scss']
})
export class BuzzInviteFormComponent extends FormComponent {
    @Input() max: number = 1;

    public formatLabel(value: number | null): any {
        return value || 0;
    }
}