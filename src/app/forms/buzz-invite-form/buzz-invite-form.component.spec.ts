import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzzInviteFormComponent } from './buzz-invite-form.component';

describe('BuzzInviteFormComponent', () => {
  let component: BuzzInviteFormComponent;
  let fixture: ComponentFixture<BuzzInviteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzzInviteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzzInviteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
