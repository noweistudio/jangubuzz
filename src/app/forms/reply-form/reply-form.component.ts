import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-reply-form',
    templateUrl: './reply-form.component.html',
    styleUrls: ['./reply-form.component.scss']
})
export class ReplyFormComponent extends FormComponent {
}