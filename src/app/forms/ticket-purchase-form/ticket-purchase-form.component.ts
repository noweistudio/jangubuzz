import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-ticket-purchase-form',
    templateUrl: './ticket-purchase-form.component.html',
    styleUrls: ['./ticket-purchase-form.component.scss']
})
export class TicketPurchaseFormComponent extends FormComponent implements OnChanges {
    @Input() model: any = {
        numberOfTickets: 1
    };
    @Input() min: number = 0;
    @Input() max: number = 100;

    ngOnChanges(changes: SimpleChanges) {
        if (changes.min && (this.model.numberOfTickets < this.min)) {
            this.model.numberOfTickets = this.min;
        }
        if (changes.max && (this.model.numberOfTickets > this.max)) {
            this.model.numberOfTickets = this.max;
        }
    }
}