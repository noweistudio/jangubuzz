import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketPurchaseFormComponent } from './ticket-purchase-form.component';

describe('TicketPurchaseFormComponent', () => {
  let component: TicketPurchaseFormComponent;
  let fixture: ComponentFixture<TicketPurchaseFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketPurchaseFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketPurchaseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
