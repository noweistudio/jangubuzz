import {Component, ViewChild} from '@angular/core';
import {FormComponent} from '../form.component';
import {HelperService} from '../../services/helper.service';
import {NgForm} from '@angular/forms';
import * as _ from 'lodash';

@Component({
    selector: 'app-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent extends FormComponent {
    @ViewChild(NgForm) form;

    get minDate(): string {
        if (!this.model._dateCreated) {
            return HelperService.getToday();
        }
        return new Date(HelperService.getToday()) < new Date(this.model._dateCreated) ? HelperService.getToday() : this.model._dateCreated;
    }

    get categoryList(): any[] {
        return HelperService.getCategoryList();
    }

    public generateCategory(): void {
        this.model.categories = {};

        _.forEach(this.model._categories, (v: string) => {
            this.model.categories[v] = true;
        });

        this.modelChange.emit(this.model);
    }
}