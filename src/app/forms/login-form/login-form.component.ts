import {Component} from "@angular/core";
import {FormComponent} from "../form.component";

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent extends FormComponent {
}