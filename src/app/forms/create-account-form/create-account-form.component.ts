import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-create-account-form',
    templateUrl: './create-account-form.component.html',
    styleUrls: ['./create-account-form.component.scss']
})
export class CreateAccountFormComponent extends FormComponent {
}