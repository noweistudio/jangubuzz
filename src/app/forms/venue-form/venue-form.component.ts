import {Component, ViewChild} from '@angular/core';
import {FormComponent} from '../form.component';
import * as firebase from 'firebase';
import GeoPoint = firebase.firestore.GeoPoint;
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-venue-form',
    templateUrl: './venue-form.component.html',
    styleUrls: ['./venue-form.component.scss']
})
export class VenueFormComponent extends FormComponent {
    @ViewChild(NgForm) form;

    get unit(): string {
        return this.model._unit;
    }

    get venueAddress(): string {
        return this.model._venueAddress;
    }

    set unit(unit: string) {
        this.model._unit = unit;
        this.generateVenueAddress();
    }

    set venueAddress(venueAddress: string)  {
        this.model._venueAddress = venueAddress;
        this.generateVenueAddress();
    }

    public setVenueAddress(place: any): void {
        this.model._venueAddress = place.formattedAddress;
        this.generateVenueAddress();
        this.model.locale = place.country;
        this.model.geoPoint = new GeoPoint(place.latitude, place.longitude);
        this.modelChange.emit(this.model);
    }

    private generateVenueAddress(): void {
        this.model.venueAddress = this.model._unit ? this.model._unit + '-' + this.model._venueAddress : this.model._venueAddress;
    }
}