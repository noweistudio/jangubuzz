import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearbyFilterFormComponent } from './nearby-filter-form.component';

describe('NearbyFilterFormComponent', () => {
  let component: NearbyFilterFormComponent;
  let fixture: ComponentFixture<NearbyFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearbyFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearbyFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
