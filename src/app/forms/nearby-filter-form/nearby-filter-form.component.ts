import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormComponent} from '../form.component';
import {HelperService} from '../../services/helper.service';

@Component({
    selector: 'app-nearby-filter-form',
    templateUrl: './nearby-filter-form.component.html',
    styleUrls: ['./nearby-filter-form.component.scss']
})
export class NearbyFilterFormComponent extends FormComponent {
    @Input() locationEnabled: boolean = true;
    @Output() sortingChange = new EventEmitter();

    get categoryList(): any[] {
        return HelperService.getCategoryList();
    }

    get moodList(): any[] {
        return HelperService.getMoodList();
    }

    public formatLabel(value: number | null): any {
        if (value >= 1000) {
            return value / 1000 + 'k';
        }

        return value || 0;
    }
}