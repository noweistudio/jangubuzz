import {EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

export abstract class FormComponent implements OnChanges {
    @Input() model: any = {};
    @Input() deactivated: {
        inputs?: Array<string>,
        buttons?: Array<string>
    } = {};
    @Input() submitBtnText: string = 'Submit';
    @Output() modelChange = new EventEmitter();
    @Output() onSubmit = new EventEmitter();
    @Output() onDelete = new EventEmitter();

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges) {
    }
}