import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-ticket-form',
    templateUrl: './ticket-form.component.html',
    styleUrls: ['./ticket-form.component.scss']
})
export class TicketFormComponent extends FormComponent {
    get purchaseable(): boolean {
        return !this.model.forShow;
    }

    set purchaseable(purchaseable: boolean) {
        this.model.forShow = !purchaseable;
    }
}