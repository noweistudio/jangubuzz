import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
  selector: 'app-recover-password-form',
  templateUrl: './recover-password-form.component.html',
  styleUrls: ['./recover-password-form.component.scss']
})
export class RecoverPasswordFormComponent extends FormComponent {
}