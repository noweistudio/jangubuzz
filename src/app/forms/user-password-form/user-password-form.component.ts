import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
  selector: 'app-user-password-form',
  templateUrl: './user-password-form.component.html',
  styleUrls: ['./user-password-form.component.scss']
})
export class UserPasswordFormComponent extends FormComponent {
}