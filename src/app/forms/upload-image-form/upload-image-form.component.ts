import {Component, Input} from '@angular/core';
import {FormComponent} from '../form.component';
import {UploadDialogComponent} from '../../dialogs/upload-dialog/upload-dialog.component';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'app-upload-image-form',
    templateUrl: './upload-image-form.component.html',
    styleUrls: ['./upload-image-form.component.scss']
})
export class UploadImageFormComponent extends FormComponent {
    @Input() title: string = 'Upload Image';

    constructor(public dialog: MatDialog) {
        super();
    }

    public openUploadDialog() {
        this.dialog.open(UploadDialogComponent, {
            width: '80%',
            minWidth: '250px',
            data: {
                dialogTitle: this.title,
                model: {
                    accept: '.png,.jpg',
                    max: 1
                }
            }
        }).afterClosed().subscribe((res) => {
            this.onSubmit.emit(res);
        });
    }
}
