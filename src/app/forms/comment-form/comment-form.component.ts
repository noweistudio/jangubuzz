import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-comment-form',
    templateUrl: './comment-form.component.html',
    styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent extends FormComponent {
}