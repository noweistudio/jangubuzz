import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
  selector: 'app-user-email-form',
  templateUrl: './user-email-form.component.html',
  styleUrls: ['./user-email-form.component.scss']
})
export class UserEmailFormComponent extends FormComponent {
}