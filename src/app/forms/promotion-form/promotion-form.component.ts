import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
    selector: 'app-promotion-form',
    templateUrl: './promotion-form.component.html',
    styleUrls: ['./promotion-form.component.scss']
})
export class PromotionFormComponent extends FormComponent {
}