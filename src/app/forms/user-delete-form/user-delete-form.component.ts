import {Component} from '@angular/core';
import {FormComponent} from '../form.component';

@Component({
  selector: 'app-user-delete-form',
  templateUrl: './user-delete-form.component.html',
  styleUrls: ['./user-delete-form.component.scss']
})
export class UserDeleteFormComponent extends FormComponent {
}