import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketBottomSheetComponent } from './ticket-bottom-sheet.component';

describe('TicketBottomSheetComponent', () => {
  let component: TicketBottomSheetComponent;
  let fixture: ComponentFixture<TicketBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
