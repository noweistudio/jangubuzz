import {Component, Inject} from '@angular/core';
import {StandardBottomSheetComponent} from '../standard-bottom-sheet/standard-bottom-sheet.component';
import {BottomSheetComponent} from '../bottom-sheet.component';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';

@Component({
    selector: 'app-ticket-bottom-sheet',
    templateUrl: './ticket-bottom-sheet.component.html',
    styleUrls: ['./ticket-bottom-sheet.component.scss']
})
export class TicketBottomSheetComponent extends BottomSheetComponent {
    constructor(public bottomSheetRef: MatBottomSheetRef<StandardBottomSheetComponent>,
                @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
        super(bottomSheetRef);
    }

    public viewReceipt() {
        this.bottomSheetRef.dismiss();
    }

    public requestRefund() {
        this.bottomSheetRef.dismiss('requestRefund');
    }
}