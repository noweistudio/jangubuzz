import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockBottomSheetComponent } from './block-bottom-sheet.component';

describe('BlockBottomSheetComponent', () => {
  let component: BlockBottomSheetComponent;
  let fixture: ComponentFixture<BlockBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
