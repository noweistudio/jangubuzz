import {Component, Inject} from '@angular/core';
import {BottomSheetComponent} from '../bottom-sheet.component';
import {StandardBottomSheetComponent} from '../standard-bottom-sheet/standard-bottom-sheet.component';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';

@Component({
    selector: 'app-block-bottom-sheet',
    templateUrl: './block-bottom-sheet.component.html',
    styleUrls: ['./block-bottom-sheet.component.scss']
})
export class BlockBottomSheetComponent extends BottomSheetComponent {
    constructor(public bottomSheetRef: MatBottomSheetRef<StandardBottomSheetComponent>,
                @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
        super(bottomSheetRef);
    }

    public block(): void {
        this.bottomSheetRef.dismiss('block');
    }

    public unblock(): void {
        this.bottomSheetRef.dismiss('unblock');
    }
}