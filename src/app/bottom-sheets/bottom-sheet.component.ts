import {MatBottomSheetRef} from '@angular/material';

export abstract class BottomSheetComponent {
    constructor(public bottomSheetRef: MatBottomSheetRef<any>) {}

    public dismiss() {
        this.bottomSheetRef.dismiss();
    }
}