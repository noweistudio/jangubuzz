import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBottomSheetComponent } from './event-bottom-sheet.component';

describe('EventBottomSheetComponent', () => {
  let component: EventBottomSheetComponent;
  let fixture: ComponentFixture<EventBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
