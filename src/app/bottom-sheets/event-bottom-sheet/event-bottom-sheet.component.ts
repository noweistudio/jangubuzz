import {Component, Inject} from '@angular/core';
import {BottomSheetComponent} from '../bottom-sheet.component';
import {StandardBottomSheetComponent} from '../standard-bottom-sheet/standard-bottom-sheet.component';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {Router} from '@angular/router';

@Component({
    selector: 'app-event-bottom-sheet',
    templateUrl: './event-bottom-sheet.component.html',
    styleUrls: ['./event-bottom-sheet.component.scss']
})
export class EventBottomSheetComponent extends BottomSheetComponent {
    constructor(public bottomSheetRef: MatBottomSheetRef<StandardBottomSheetComponent>,
                public router: Router,
                @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
        super(bottomSheetRef);
    }

    public sendBuzzInvite(): void {
        this.bottomSheetRef.dismiss('invite');
    }

    public edit(): void {
        this.bottomSheetRef.dismiss('edit');
    }

    public viewTicketTimeline(): void {
        this.bottomSheetRef.dismiss('viewTicketTimeline');
    }

    public delete(): void {
        this.bottomSheetRef.dismiss('delete');
    }
}