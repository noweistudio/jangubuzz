import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsBottomSheetComponent } from './tickets-bottom-sheet.component';

describe('TicketsBottomSheetComponent', () => {
  let component: TicketsBottomSheetComponent;
  let fixture: ComponentFixture<TicketsBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
