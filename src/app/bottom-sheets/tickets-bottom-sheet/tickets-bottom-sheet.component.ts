import {Component, Inject} from '@angular/core';
import {StandardBottomSheetComponent} from '../standard-bottom-sheet/standard-bottom-sheet.component';
import {BottomSheetComponent} from '../bottom-sheet.component';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {Observable} from 'rxjs';
import {ListingTicketService} from '../../services/listing-ticket.service';

@Component({
    selector: 'app-tickets-bottom-sheet',
    templateUrl: './tickets-bottom-sheet.component.html',
    styleUrls: ['./tickets-bottom-sheet.component.scss']
})
export class TicketsBottomSheetComponent extends BottomSheetComponent {
    public listingTicketsObservable: Observable<any>;

    constructor(public bottomSheetRef: MatBottomSheetRef<StandardBottomSheetComponent>,
                @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
                public listingTicketService: ListingTicketService) {
        super(bottomSheetRef);

        this.listingTicketsObservable = this.listingTicketService.index(this.data.eventId);
    }
}