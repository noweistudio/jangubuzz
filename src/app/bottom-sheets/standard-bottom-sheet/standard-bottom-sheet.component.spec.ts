import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardBottomSheetComponent } from './standard-bottom-sheet.component';

describe('StandardBottomSheetComponent', () => {
  let component: StandardBottomSheetComponent;
  let fixture: ComponentFixture<StandardBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
