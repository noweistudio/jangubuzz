import {Component} from '@angular/core';
import {BottomSheetComponent} from '../bottom-sheet.component';
import {MatBottomSheetRef} from '@angular/material';

@Component({
    selector: 'app-standard-bottom-sheet',
    templateUrl: './standard-bottom-sheet.component.html',
    styleUrls: ['./standard-bottom-sheet.component.scss']
})
export class StandardBottomSheetComponent extends BottomSheetComponent{
    constructor(public bottomSheetRef: MatBottomSheetRef<StandardBottomSheetComponent>) {
        super(bottomSheetRef);
    }

    public delete() {
        this.bottomSheetRef.dismiss('delete');
    }
}