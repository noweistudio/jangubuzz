import {Component, HostListener} from '@angular/core';
import {take} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PageComponent} from '../page.component';
import {ListingService} from '../../services/listing.service';
import {ListingTicketService} from '../../services/listing-ticket.service';
import {environment} from '../../../environments/environment';
import {PaymentService} from '../../services/payment.service';

@Component({
    selector: 'app-ticket-purchase-page',
    templateUrl: './ticket-purchase-page.component.html',
    styleUrls: ['./ticket-purchase-page.component.scss']
})
export class TicketPurchasePageComponent extends PageComponent {
    public id: string;
    public eventId: string;
    public payload: any = {
        type : 'ticketPurchase',
        ticketId : '',
        listingId : '',
        numberOfTickets: 1,
        maxNumber: 1,
        currencyCode: 'USD'
    };
    public model: any = {};
    public modelObservable: Observable<any>;
    public listingObservable: Observable<any>;

    private handler: any;
    private multiplier: number = 1;

    constructor(public activatedRoute: ActivatedRoute,
                public router: Router,
                public listingService: ListingService,
                public listingTicketService: ListingTicketService,
                public paymentService: PaymentService) {
        super();

        this.activatedRoute.params.subscribe(
            (params: Params) => {
                this.id = params['id'];
                this.eventId = params['eventId'];
                this.payload.ticketId = this.id;
                this.payload.listingId = this.eventId;
            }
        );

        this.modelObservable = this.listingTicketService.read(this.eventId, this.id);
        this.listingObservable = this.listingService.read(this.eventId);

        this.modelObservable.pipe(take(1)).subscribe((res) => {
            this.model = res;
            this.payload.maxNumber = this.model.maxNumber;
        });

        this.listingObservable.pipe(take(1)).subscribe((res) => {
            if (res.locale.includes('CA')) {
                this.payload.currencyCode = 'cad';
            } else {
                this.payload.currencyCode = 'usd';
            }

            this.handler = StripeCheckout.configure({
                key: environment.stripeKey,
                locale: 'auto',
                currency: this.payload.currencyCode,
                token: token => {
                    this.paymentService.purchaseTicket(this.payload, token.id).subscribe((res)=>{
                        this.router.navigateByUrl('/page/tickets');
                    }, (err)=>{
                        console.log(err);
                    });
                }
            });
        });
    }

    get amount(): number {
        return this.payload.numberOfTickets * this.model.price * this.multiplier;
    }

    public handlePayment(): void {
        if (this.payload.currencyCode === 'cad') {
            this.multiplier = 100;
        } else if (this.payload.currencyCode === 'usd') {
            this.multiplier = 100;
        }

        if (this.model.price === 0) {
            this.paymentService.purchaseFreeTicket(this.payload).subscribe((res)=>{
                this.router.navigateByUrl('/page/tickets');
            }, (err)=>{
                console.log(err);
            });
        } else {
            this.handler.open({
                amount: this.amount
            });
        }
    }

    @HostListener('window:popstate')
    onPopstate() {
        this.handler.close();
    }
}