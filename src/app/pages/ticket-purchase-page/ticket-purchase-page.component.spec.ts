import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketPurchasePageComponent } from './ticket-purchase-page.component';

describe('TicketPurchasePageComponent', () => {
  let component: TicketPurchasePageComponent;
  let fixture: ComponentFixture<TicketPurchasePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketPurchasePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketPurchasePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
