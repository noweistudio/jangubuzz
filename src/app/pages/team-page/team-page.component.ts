import {Component} from '@angular/core';
import {MatBottomSheet} from '@angular/material';
import {TeamService} from '../../services/team.service';
import {PageComponent} from '../page.component';
import {StandardBottomSheetComponent} from '../../bottom-sheets/standard-bottom-sheet/standard-bottom-sheet.component';
import {QueryDocumentSnapshot} from 'angularfire2/firestore';

@Component({
    selector: 'app-team-page',
    templateUrl: './team-page.component.html',
    styleUrls: ['./team-page.component.scss']
})
export class TeamPageComponent extends PageComponent {
    constructor(public teamService: TeamService,
                protected bottomSheet: MatBottomSheet) {
        super();
    }

    public openTeamMemberActionBottomSheet(teamMember: QueryDocumentSnapshot<any>): void {
        let bottomSheetRef = this.bottomSheet.open(StandardBottomSheetComponent, {});

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'delete':
                    this.teamService.removeTeamMember(teamMember);
                    break;
                default:
            }
        });
    }
}