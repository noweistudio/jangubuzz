import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends PageComponent{
    public user: any = {};

    constructor(public authService: AuthService) {
        super();
    }
}