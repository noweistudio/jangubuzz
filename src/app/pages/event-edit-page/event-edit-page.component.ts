import {Component, ViewChild} from '@angular/core';
import {ReadPageComponent} from '../read-page.component';
import {ListingService} from '../../services/listing.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HelperService} from '../../services/helper.service';
import {VenueFormComponent} from '../../forms/venue-form/venue-form.component';
import {EventFormComponent} from '../../forms/event-form/event-form.component';
import {MatDialog} from '@angular/material';
import {ListingTicketService} from '../../services/listing-ticket.service';
import {TicketEditDialogComponent} from '../../dialogs/ticket-edit-dialog/ticket-edit-dialog.component';
import {take} from 'rxjs/internal/operators';
import * as _ from 'lodash';

@Component({
    selector: 'app-event-edit-page',
    templateUrl: './event-edit-page.component.html',
    styleUrls: ['./event-edit-page.component.scss']
})
export class EventEditPageComponent extends ReadPageComponent {
    @ViewChild(EventFormComponent) eventFormComponent: EventFormComponent;
    @ViewChild(VenueFormComponent) venueFormComponent: VenueFormComponent;

    public model: any = HelperService.getDefaultEvent();
    public tickets: any[] = [];
    public ticketPromises: any[] = [];

    constructor(public activatedRoute: ActivatedRoute,
                public listingService: ListingService,
                public dialog: MatDialog,
                public listingTicketService: ListingTicketService,
                protected router: Router) {
        super(activatedRoute, listingService);

        this.id = this.id || this.listingService.createId();
    }

    get eventFormCompleted(): boolean {
        return !!(this.model.profileImageUrl && this.eventFormComponent.form.valid);
    }

    public updateImage(payloadSet: Set<any>): void {
        if (!payloadSet) {
            return;
        }

        this.listingService.createListingImage(this.id, payloadSet.values().next().value).then((res: any[]) => {
            Promise.all([
                res[0].ref.getDownloadURL(),
                res[1].ref.getDownloadURL()
            ]).then((res) => {
                this.model.profileImageUrl = res[0];
                this.model.smallProfileImageUrl = res[1];

                let img = new Image();
                img.onload = () => {
                    if (img.width > img.height) {
                        this.model.imageOrientation = 1;
                    } else if (img.height > img.width) {
                        this.model.imageOrientation = 2;
                    } else {
                        this.model.imageOrientation = 0;
                    }
                };
                img.src = this.model.profileImageUrl;
            }).catch((err) => {
                throw err;
            });
        }).catch((err) => {
            console.log(err);
        });
    }

    public submit(): void {
        if (this.model.dateCreated) {
            this.updateEvent();
        } else {
            this.createEvent();
        }
    }

    public createEvent(): void {
        let promiseArray: any[] = [
            () => this.listingService.create(this.id, this.model)
        ];

        this.processEvent(promiseArray);
    }

    public updateEvent(): void {
        let promiseArray: any[] = [
            () => this.listingService.update(this.id, this.model)
        ];

        this.processEvent(promiseArray);
    }

    public createTicket(ticket: any): void {
        let index: number = _.findIndex(this.tickets, (v: any) => {
            return v.id === ticket.id;
        });

        ticket.locale = this.model.locale;

        if (index === -1) {
            this.tickets.push(ticket);
        } else {
            this.tickets[index] = ticket;
        }

        this.ticketPromises[ticket.id] = () => this.listingTicketService.create(this.id, ticket.id, ticket);
    }

    public updateTicket(ticket: any): void {
        let index: number = _.findIndex(this.tickets, (v: any) => {
            return v.id === ticket.id;
        });

        ticket.locale = this.model.locale;

        if (index === -1) {
            this.tickets.push(ticket);
        } else {
            this.tickets[index] = ticket;
        }

        this.ticketPromises[ticket.id] = () => this.listingTicketService.update(this.id, ticket.id, ticket);
    }

    public removeTicket(ticket: any): void {
        _.remove(this.tickets, (v: any) => {
            return v.id === ticket.id;
        });

        if (ticket.dateCreated) {
            this.ticketPromises[ticket.id] = () => this.listingTicketService.delete(this.id, ticket.id);
        } else {
            delete this.ticketPromises[ticket.id];
        }
    }

    public openTicketEditDialog(ticket: any): void {
        this.dialog.open(TicketEditDialogComponent, {
            width: '80%',
            minWidth: '250px',
            data: {
                dialogTitle: '',
                model: Object.assign(HelperService.getDefaultTicket(), ticket)
            }
        }).afterClosed().subscribe((res) => {
            if (res) {
                if (res.dateCreated) {
                    this.updateTicket(res);
                } else {
                    this.createTicket(res);
                }
            }
        });
    }

    protected modelObservableCallback(): void {
        this.listingTicketService.index(this.id).pipe(take(1)).subscribe((res) => {
            _.forEach(res, (v: any) => {
                let ticket = v.payload.doc.data();
                ticket.id = v.payload.doc.id;
                this.tickets.push(ticket);
            });
        });
    }

    private processEvent(promiseArray: any[]): void {
        // model
        this.model.priceArray = [];

        _.forEach(this.tickets, (v: any) => {
            this.model.priceArray.push(v.price);
        });

        // tickets
        _.forOwn(this.ticketPromises, (v: any) => {
            promiseArray.push(v);
        });

        // submit
        Promise.all(promiseArray.map(f => f())).then((res) => {
            this.router.navigateByUrl('/page/event/' + this.id);
        }).catch((err) => {
            console.log(err);
        });
    }
}
