import {Component, ViewChild} from '@angular/core';
import {PageComponent} from '../page.component';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {ListingService} from '../../services/listing.service';
import {UserListingBookmarkedService} from '../../services/user-listing-bookmarked.service';
import {UserListingGoingService} from '../../services/user-listing-going.service';
import {Observable} from 'rxjs/index';
import {HelperService} from '../../services/helper.service';
import {EventFormComponent} from '../../forms/event-form/event-form.component';
import {VenueFormComponent} from '../../forms/venue-form/venue-form.component';

@Component({
    selector: 'app-my-events-page',
    templateUrl: './my-events-page.component.html',
    styleUrls: ['./my-events-page.component.scss']
})
export class MyEventsPageComponent extends PageComponent {
    @ViewChild(EventFormComponent) eventFormComponent: EventFormComponent;
    @ViewChild(VenueFormComponent) venueFormComponent: VenueFormComponent;

    public id: string = this.listingService.createId();
    public model: any = HelperService.getDefaultEvent();
    public tickets: any[] = [];
    public currentUserHostingListingsObservable: Observable<any>;
    public currentUserBookmarkedListingsObservable: Observable<any>;
    public currentUserGoingListingsObservable: Observable<any>;

    constructor(public router: Router,
                public dialog: MatDialog,
                public listingService: ListingService,
                public userListingBookmarkedService: UserListingBookmarkedService,
                public userListingGoingService: UserListingGoingService) {
        super();

        this.currentUserHostingListingsObservable = this.listingService.indexCurrentUserHosting();
        this.currentUserBookmarkedListingsObservable = this.userListingBookmarkedService.index();
        this.currentUserGoingListingsObservable = this.userListingGoingService.index();
    }
}