import {MatBottomSheet, MatDialog, MatDialogConfig} from '@angular/material';
import {Router} from '@angular/router';
import {LoadingService} from '../services/loading.service';
import {OnDestroy} from '@angular/core';

export abstract class PageComponent implements OnDestroy {
    protected router: Router;
    protected bottomSheet: MatBottomSheet;
    protected dialog: MatDialog;
    protected loadingService: LoadingService = new LoadingService(); // todo move to constructor

    constructor() {
    }

    /**
     * Generate angular material dialog config object
     * @returns {MatDialogConfig}
     */
    public static generateMatDialogConfig(): MatDialogConfig {
        let config = new MatDialogConfig();
        config.width = '600px';

        return config;
    }

    ngOnDestroy(): void {
        this.loadingService.clear();
    }
}