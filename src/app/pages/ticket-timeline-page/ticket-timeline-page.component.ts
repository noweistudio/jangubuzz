import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRoute, Params} from '@angular/router';
import {PageComponent} from '../page.component';
import {ListingMoneyMadeService} from '../../services/listing-money-made.service';

@Component({
    selector: 'app-ticket-timeline-page',
    templateUrl: './ticket-timeline-page.component.html',
    styleUrls: ['./ticket-timeline-page.component.scss']
})
export class TicketTimelinePageComponent extends PageComponent {
    public id: string;
    public model: any;
    public modelObservable: Observable<any>;

    constructor(public activatedRoute: ActivatedRoute,
                public listingMoneyMadeService: ListingMoneyMadeService) {
        super();

        this.activatedRoute.params.subscribe(
            (params: Params) => {
                this.id = params['id'];
            }
        );

        if (this.id) {
            this.modelObservable = this.listingMoneyMadeService.index(this.id);
        }
    }
}