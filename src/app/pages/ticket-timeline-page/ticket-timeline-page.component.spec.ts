import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketTimelinePageComponent } from './ticket-timeline-page.component';

describe('TicketTimelinePageComponent', () => {
  let component: TicketTimelinePageComponent;
  let fixture: ComponentFixture<TicketTimelinePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketTimelinePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketTimelinePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
