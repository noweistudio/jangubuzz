import {Component} from '@angular/core';
import {ReadPageComponent} from '../read-page.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ListingService} from '../../services/listing.service';
import {ListingGoingService} from '../../services/listing-going.service';
import {Observable} from 'rxjs/index';
import {ListingCommentService} from '../../services/listing-comment.service';
import {UserService} from '../../services/user.service';
import {UserListingGoingService} from '../../services/user-listing-going.service';
import {AuthService} from '../../services/auth.service';
import {MatBottomSheet, MatDialog} from '@angular/material';
import {EventBottomSheetComponent} from '../../bottom-sheets/event-bottom-sheet/event-bottom-sheet.component';
import {TicketsBottomSheetComponent} from '../../bottom-sheets/tickets-bottom-sheet/tickets-bottom-sheet.component';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';

@Component({
    selector: 'app-event-page',
    templateUrl: './event-page.component.html',
    styleUrls: ['./event-page.component.scss']
})
export class EventPageComponent extends ReadPageComponent {
    public listingCommentsObservable: Observable<any>;
    public listingGoingObservable: Observable<any>;
    public listingHostObservable: Observable<any>;
    public currentUserIsGoingObservable: Observable<any>;

    private _zoomIn: boolean = false;

    constructor(public activatedRoute: ActivatedRoute,
                public listingService: ListingService,
                public listingCommentService: ListingCommentService,
                public listingGoingService: ListingGoingService,
                public userService: UserService,
                public userListingGoingService: UserListingGoingService,
                public authService: AuthService,
                protected router: Router,
                protected bottomSheet: MatBottomSheet,
                protected dialog: MatDialog) {
        super(activatedRoute, listingService);

        this.listingCommentsObservable = this.listingCommentService.index(this.id);
        this.listingGoingObservable = this.listingGoingService.index(this.id);
        this.currentUserIsGoingObservable = this.userListingGoingService.currentUserIsGoing(this.id);
    }

    get isHostedByCurrentUser(): boolean {
        if (!this.model || !this.authService.user) {
            return false;
        }
        return this.model.userId === this.authService.user.uid;
    }

    get zoomIn(): boolean {
        return this._zoomIn;
    }

    set zoomIn(bool: boolean) {
        this._zoomIn = bool;
    }

    public join(): void {
        Promise.all([
            this.listingGoingService.joinAsCurrentUser(this.id),
            this.userListingGoingService.joinAsCurrentUser(this.id)
        ]);
    }

    public unjoin(): void {
        Promise.all([
            this.listingGoingService.unjoinAsCurrentUser(this.id),
            this.userListingGoingService.unjoinAsCurrentUser(this.id)
        ]);
    }

    public openEventBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(EventBottomSheetComponent, {
            data: {
                eventId: this.id,
                disabled: {
                    edit: !this.isHostedByCurrentUser,
                    viewTicketTimeline: !this.isHostedByCurrentUser,
                    delete: !!this.model.ticketsCount
                }
            }
        });

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'invite':
                    this.router.navigateByUrl('/page/buzz-invite/' + this.id);
                    break;
                case 'edit':
                    this.router.navigateByUrl('/page/event-edit/' + this.id);
                    break;
                case 'viewTicketTimeline':
                    this.router.navigateByUrl('/page/ticket-timeline/' + this.id);
                    break;
                case 'delete':
                    this.openDeleteEventConfirmationDialog();
                    break;
                default:
            }
        });
    }

    public openTicketsBottomSheet(): void {
        let bottomSheetRef = this.bottomSheet.open(TicketsBottomSheetComponent, {
            data: {
                eventId: this.id
            }
        });

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                default:
            }
        });
    }

    public openDeleteEventConfirmationDialog(): void {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '300px',
            maxWidth: '80%',
            data: {
                dialogTitle: 'Delete Event Confirmation',
                model: {
                    confirmation: 'Are you sure you want to delete this event?'
                }
            }
        }).afterClosed().subscribe((res) => {
            switch (res) {
                case 'confirm':
                    this.listingService.delete(this.id).then(() => {
                        this.router.navigateByUrl('/page/events');
                    });
                    break;
                default:
            }
        });
    }

    public isEnded(date: any): boolean {
        return new Date(date) < new Date();
    }

    protected modelObservableCallback(): void {
        this.listingHostObservable = this.userService.read(this.model.userId);
    }
}