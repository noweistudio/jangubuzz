import {Component} from '@angular/core';
import {PageComponent} from '../page.component';

@Component({
  selector: 'app-activities-page',
  templateUrl: './activities-page.component.html',
  styleUrls: ['./activities-page.component.scss']
})
export class ActivitiesPageComponent extends PageComponent {
}