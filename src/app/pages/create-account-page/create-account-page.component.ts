import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {PageComponent} from '../page.component';

@Component({
    selector: 'app-create-account-page',
    templateUrl: './create-account-page.component.html',
    styleUrls: ['./create-account-page.component.scss']
})
export class CreateAccountPageComponent extends PageComponent {
    public user: any = {};

    constructor(public router: Router,
                public authService: AuthService) {
        super();
    }
}