import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';

@Component({
    selector: 'app-settings-page',
    templateUrl: './settings-page.component.html',
    styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent extends PageComponent {
    public model: any = {};

    constructor(public dialog: MatDialog,
                public authService: AuthService) {
        super();
    }

    public deleteUser(): void {
        this.dialog.open(ConfirmationDialogComponent, {
            width: '300px',
            maxWidth: '80%',
            data: {
                dialogTitle: 'Delete Account Confirmation',
                model: {
                    confirmation: 'Are you sure you want to delete your account?'
                }
            }
        }).afterClosed().subscribe((res) => {
            switch (res) {
                case 'confirm':
                    this.authService.deleteAcount(this.model.password);
                    break;
                default:
            }
        });
    }
}