import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {UserTicketService} from '../../services/user-ticket.service';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-my-tickets-page',
    templateUrl: './my-tickets-page.component.html',
    styleUrls: ['./my-tickets-page.component.scss']
})
export class MyTicketsPageComponent extends PageComponent {
    public currentUserTicketsObservable: Observable<any>;

    constructor(public userTicketService: UserTicketService) {
        super();

        this.currentUserTicketsObservable = this.userTicketService.index();
    }
}