import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzzInvitePageComponent } from './buzz-invite-page.component';

describe('BuzzInvitePageComponent', () => {
  let component: BuzzInvitePageComponent;
  let fixture: ComponentFixture<BuzzInvitePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzzInvitePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzzInvitePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
