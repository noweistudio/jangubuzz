import {Component} from '@angular/core';
import {ReadPageComponent} from '../read-page.component';
import {ActivatedRoute} from '@angular/router';
import {ListingService} from '../../services/listing.service';
import {Observable} from 'rxjs';
import {BuzzService} from '../../services/buzz.service';
import {UserService} from '../../services/user.service';
import {UserBuzzSendService} from '../../services/user-buzz-send.service';
import {HelperService} from '../../services/helper.service';
import {take} from 'rxjs/internal/operators';

@Component({
    selector: 'app-buzz-invite-page',
    templateUrl: './buzz-invite-page.component.html',
    styleUrls: ['./buzz-invite-page.component.scss']
})
export class BuzzInvitePageComponent extends ReadPageComponent {
    public buzzInvite: any = HelperService.getDefaultBuzzInvite();
    public buzzCreditObservable: Observable<any>;

    constructor(public activatedRoute: ActivatedRoute,
                public listingService: ListingService,
                public buzzService: BuzzService,
                public userService: UserService,
                public userBuzzSendService: UserBuzzSendService) {
        super(activatedRoute, listingService);

        this.buzzCreditObservable = this.buzzService.indexAsCurrentUser();
    }

    public sendBuzzInvite(): void {
        this.buzzInvite.useBonus = this.buzzInvite.buzzRadius <= 1;

        this.userBuzzSendService.sendBuzzInviteAsCurrentUser(this.userBuzzSendService.createId(), this.buzzInvite);
    }

    protected modelObservableCallback(): void {
        Object.assign(this.buzzInvite, {
            listingId: this.id,
            eventName: this.model.name,
            eventPic: this.model.profileImageUrl,
            eventPrice: this.model.priceArray.length > 0 ? this.model.priceArray[0] : 0,
            eventStartDate: this.model.startDate,
            eventCategories: this.model.categories || {},
            eventMoods: this.model.moods || {},
            hostId: this.model.userId,
            locale: this.model.locale,
            latitude: this.model.geoPoint.latitude,
            longitude: this.model.geoPoint.longitude
        });

        if (this.id) {
            this.modelObservable = this.crudService.read(this.id);
            this.userService.read(this.model.userId).pipe(take(1)).subscribe((res) => {
                Object.assign(this.buzzInvite, {
                    hostName: res.name,
                    hostPic: res.profileImageUrl
                });
            });
        }
    }
}