import {Component, ViewChild} from '@angular/core';
import {PageComponent} from '../page.component';
import {AuthService} from '../../services/auth.service';
import {UserService} from '../../services/user.service';
import {UserFormComponent} from '../../forms/user-form/user-form.component';

@Component({
    selector: 'app-user-page',
    templateUrl: './user-page.component.html',
    styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent extends PageComponent {
    @ViewChild(UserFormComponent) userFormComponent: UserFormComponent;

    constructor(public authService: AuthService,
                public userService: UserService) {
        super();
    }

    public updateImage(payloadSet: Set<any>): void {
        if (!payloadSet) {
            return;
        }

        this.userService.updateCurrentUserImage(payloadSet.values().next().value).then((res: any[]) => {
            Promise.all([
                res[0].ref.getDownloadURL(),
                res[1].ref.getDownloadURL()
            ]).then((res) => {
                this.authService.userData.profileImageUrl = res[0];
                this.authService.userData.smallProfileImageUrl = res[1];

                this.userFormComponent.form.form.markAsDirty();
            }).catch((err) => {
                throw err;
            });
        }).catch((err) => {
            console.log(err);
        });
    }

    public update(): void {
        this.userService.updateCurrentUser(this.authService.userData);
    }
}