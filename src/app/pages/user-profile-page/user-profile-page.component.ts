import {Component} from '@angular/core';
import {ReadPageComponent} from '../read-page.component';
import {ActivatedRoute} from '@angular/router';
import {MatBottomSheet} from '@angular/material';
import {BlockBottomSheetComponent} from '../../bottom-sheets/block-bottom-sheet/block-bottom-sheet.component';
import {Observable} from 'rxjs/index';
import {UserService} from '../../services/user.service';
import {ListingService} from '../../services/listing.service';
import {UserBlockService} from '../../services/user-block.service';
import {UserFollowerService} from '../../services/user-follower.service';
import {UserFollowingService} from '../../services/user-following.service';

@Component({
    selector: 'app-user-profile-page',
    templateUrl: './user-profile-page.component.html',
    styleUrls: ['./user-profile-page.component.scss']
})
export class UserProfilePageComponent extends ReadPageComponent {
    public userListingsObservable: Observable<any>;
    public userFollowersObservable: Observable<any>;
    public userFollowingsObservable: Observable<any>;
    public currentUserIsFollowingObservable: Observable<any>;
    public currentUserIsBlockingObservable: Observable<any>;

    constructor(public activatedRoute: ActivatedRoute,
                public userService: UserService,
                public listingService: ListingService,
                public userBlockService: UserBlockService,
                public userFollowerService: UserFollowerService,
                public userFollowingService: UserFollowingService,
                protected bottomSheet: MatBottomSheet) {
        super(activatedRoute, userService);


        this.userListingsObservable = this.listingService.indexUserHosting(this.id);
        this.userFollowersObservable = this.userFollowerService.index(this.id);
        this.userFollowingsObservable = this.userFollowingService.index(this.id);
        this.currentUserIsFollowingObservable = this.userFollowingService.currentUserIsFollowing(this.id);
        this.currentUserIsBlockingObservable = this.userBlockService.currentUserIsBlocking(this.id);
    }

    public follow(): void {
        Promise.all([
            this.userFollowerService.followAsCurrentUser(this.id),
            this.userFollowingService.followAsCurrentUser(this.id, this.model)
        ]);
    }

    public unfollow(): void {
        Promise.all([
            this.userFollowerService.unfollowAsCurrentUser(this.id),
            this.userFollowingService.unfollowAsCurrentUser(this.id)
        ]);
    }

    public block(): void {
        Promise.all([
            this.userBlockService.blockAsCurrentUser(this.id),
            this.userFollowerService.unfollowAsCurrentUser(this.id),
            this.userFollowingService.unfollowAsCurrentUser(this.id)
        ]);
    }

    public unblock(): void {
        Promise.all([
            this.userBlockService.unblockAsCurrentUser(this.id)
        ]);
    }

    public openBlockBottomSheet(data: any = {}): void {
        let bottomSheetRef = this.bottomSheet.open(BlockBottomSheetComponent, {
            data: data
        });

        bottomSheetRef.afterDismissed().subscribe((res) => {
            switch (res) {
                case 'block':
                    this.block();
                    break;
                case 'unblock':
                    this.unblock();
                    break;
                default:
            }
        });
    }
}