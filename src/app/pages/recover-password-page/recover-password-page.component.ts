import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {PageComponent} from '../page.component';

@Component({
    selector: 'app-recover-password-page',
    templateUrl: './recover-password-page.component.html',
    styleUrls: ['./recover-password-page.component.scss']
})
export class RecoverPasswordPageComponent extends PageComponent {
    public user: any = {};

    constructor(public router: Router,
                public authService: AuthService) {
        super();
    }

    /**
     * Recover Password
     */
    public recoverPassword() {
        this.authService.recoverPassword(this.user);
    }
}