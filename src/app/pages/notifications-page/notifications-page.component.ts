import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {NotificationService} from '../../services/notification.service';

@Component({
    selector: 'app-notifications-page',
    templateUrl: './notifications-page.component.html',
    styleUrls: ['./notifications-page.component.scss']
})
export class NotificationsPageComponent extends PageComponent {
    constructor(public notificationService: NotificationService) {
        super();
    }
}