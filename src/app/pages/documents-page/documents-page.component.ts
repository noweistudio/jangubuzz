import {Component} from '@angular/core';
import {PageComponent} from '../page.component';

@Component({
  selector: 'app-documents-page',
  templateUrl: './documents-page.component.html',
  styleUrls: ['./documents-page.component.scss']
})
export class DocumentsPageComponent extends PageComponent {
}