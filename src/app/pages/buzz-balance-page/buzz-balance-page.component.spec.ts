import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuzzBalancePageComponent } from './buzz-balance-page.component';

describe('BuzzBalancePageComponent', () => {
  let component: BuzzBalancePageComponent;
  let fixture: ComponentFixture<BuzzBalancePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuzzBalancePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuzzBalancePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
