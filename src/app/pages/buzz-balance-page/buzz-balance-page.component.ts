import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {BuzzService} from '../../services/buzz.service';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-buzz-balance-page',
    templateUrl: './buzz-balance-page.component.html',
    styleUrls: ['./buzz-balance-page.component.scss']
})
export class BuzzBalancePageComponent extends PageComponent {
    public buzzCreditObservable: Observable<any>;

    constructor(public buzzService: BuzzService) {
        super();

        this.buzzCreditObservable = this.buzzService.indexAsCurrentUser();
    }
}