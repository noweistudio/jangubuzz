import {Component} from '@angular/core';
import {PageComponent} from '../page.component';
import {ActivatedRoute, Params} from '@angular/router';
import {take} from 'rxjs/internal/operators';
import {Observable} from 'rxjs';
import {UserReceiptService} from '../../services/user-receipt.service';

@Component({
    selector: 'app-receipt-page',
    templateUrl: './receipt-page.component.html',
    styleUrls: ['./receipt-page.component.scss']
})
export class ReceiptPageComponent extends PageComponent {
    public id: string;
    public modelObservable: Observable<any>;

    constructor(public activatedRoute: ActivatedRoute,
                public userReceiptService: UserReceiptService) {
        super();

        this.activatedRoute.params.subscribe(
            (params: Params) => {
                this.id = params['id'];
            }
        );

        this.modelObservable = this.userReceiptService.readAsCurrentUser(this.id);
    }
}