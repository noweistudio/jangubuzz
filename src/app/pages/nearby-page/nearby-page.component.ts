import {Component} from '@angular/core';
import {map} from 'rxjs/internal/operators';
import {concat, Observable} from 'rxjs';
import {PageComponent} from '../page.component';
import {UserNearbyService} from '../../services/user-nearby.service';
import {HelperService} from '../../services/helper.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-nearby-page',
    templateUrl: './nearby-page.component.html',
    styleUrls: ['./nearby-page.component.scss']
})
export class NearbyPageComponent extends PageComponent {
    public nearbyFilter: any = {
        categories: [],
        moods: [],
        radius: 500,
        min: 0,
        max: 10000,
        date: 'future',
        sorting: 'date'
    };
    public currentCoordinate: any = {};
    public currentUserNearbyListingsObservable: Observable<any>;

    constructor(public userNearbyService: UserNearbyService) {
        super();

        this.initializeCurrentLocation();
    }

    public updateCurrentUserNearbyListingsObservable(): void {
        switch (this.nearbyFilter.sorting) {
            case 'distance':
                if (!this.currentCoordinate) {
                    this.currentUserNearbyListingsObservable = this.userNearbyService.index();
                    break;
                }

                this.currentUserNearbyListingsObservable = this.userNearbyService.index().pipe(map((res: any[]) => {
                    res = _.orderBy(res, (v) => {
                        return this.calculateDistance(
                            v.payload.doc.data().geoPoint.latitude,
                            this.currentCoordinate.latitude,
                            v.payload.doc.data().geoPoint.longitude,
                            this.currentCoordinate.longitude);
                    }, 'asc');
                    return res;
                }));
                break;
            case 'heat':
                this.currentUserNearbyListingsObservable = this.userNearbyService.index().pipe(map((res: any[]) => {
                    res = _.orderBy(res, (v) => {
                        let weights: number = v.payload.doc.data().viewsCount * HelperService.getHeatLevelWeights().VIEW +
                            v.payload.doc.data().bookmarksCount * HelperService.getHeatLevelWeights().BOOKMARK +
                            v.payload.doc.data().commentsCount * HelperService.getHeatLevelWeights().COMMENT +
                            v.payload.doc.data().repliesCount * HelperService.getHeatLevelWeights().REPLY +
                            v.payload.doc.data().goingCount * HelperService.getHeatLevelWeights().GOING +
                            v.payload.doc.data().ticketsCount * HelperService.getHeatLevelWeights().TICKET;

                        let days: number = (HelperService.getNow().getTime() - v.payload.doc.data().dateCreated.seconds * 1000) / HelperService.getDayUnits().MILLISECONDS;

                        return weights / days;
                    }, 'desc');
                    return res;
                }));
                break;
            case 'price':
                this.currentUserNearbyListingsObservable = this.userNearbyService.index().pipe(map((res: any[]) => {
                    res = _.orderBy(res, (v) => {
                        if (v.payload.doc.data().isFree) {
                            return -2;
                        }
                        if (!v.payload.doc.data().priceArray) {
                            return -1;
                        }
                        return v.payload.doc.data().priceArray[0];
                    }, 'asc');
                    return res;
                }));
                break;
            default:
                this.currentUserNearbyListingsObservable = this.userNearbyService.index().pipe(map((res: any[]) => {
                    res = _.orderBy(res, (v) => {
                        return v.payload.doc.data().startDate.seconds;
                    }, 'asc');
                    return res;
                }));
        }
    }

    public isMatchNearbyFilter(listing: any): boolean {
        return !listing.isCanceled && this.isMatchCategory(listing) && this.isMatchMood(listing) && this.isMatchRadius(listing) && this.isMatchPriceRange(listing) && this.isMatchDate(listing);
    }

    private initializeCurrentLocation(): void {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((res) => {
                this.currentCoordinate = res.coords;

                concat(this.userNearbyService.deleteCurrentUserNearby(),
                    this.userNearbyService.updateCurrentUserNearby(res.coords.longitude, res.coords.latitude)).subscribe(() => {
                    this.updateCurrentUserNearbyListingsObservable();
                    console.log('success update location');
                }, () => {
                    this.updateCurrentUserNearbyListingsObservable();
                    console.log('failed update location at reload nearby');
                });
            }, () => {
                this.currentCoordinate = null;
                this.updateCurrentUserNearbyListingsObservable();
                console.log('failed update location at invalid geo location');
            });
        } else {
            this.currentCoordinate = null;
            this.updateCurrentUserNearbyListingsObservable();
            console.log('failed update location at no geo location');
        }
    }

    private isMatchCategory(listing: any): boolean {
        if (this.nearbyFilter.categories.length === 0) {
            return true;
        }

        return _.findIndex(this.nearbyFilter.categories, (v: string) => {
            return listing.categories[v];
        }) != -1;
    }

    private isMatchMood(listing: any): boolean {
        if (this.nearbyFilter.moods.length === 0) {
            return true;
        }

        return _.findIndex(this.nearbyFilter.moods, (v: string) => {
            return listing.moods[v];
        }) != -1;
    }

    private isMatchRadius(listing: any): boolean {
        if (!this.currentCoordinate || !listing.geoPoint) {
            return true;
        }

        return this.calculateDistance(
            listing.geoPoint.latitude,
            this.currentCoordinate.latitude,
            listing.geoPoint.longitude,
            this.currentCoordinate.longitude) <= this.nearbyFilter.radius;
    }

    private isMatchPriceRange(listing: any): boolean {
        if (listing.isFree || (listing.priceArray && listing.priceArray.length === 0)) {
            return this.nearbyFilter.min === 0;
        }

        return _.findIndex(listing.priceArray, (v: number) => {
            return (v >= this.nearbyFilter.min) && (v <= this.nearbyFilter.max);
        }) != -1;
    }

    private isMatchDate(listing: any): boolean {
        let startDate: Date;
        let endDate: Date;

        if (!listing.startDate || !listing.startDate.seconds ||
            !listing.endDate || !listing.endDate.seconds) {
            return false;
        }

        startDate = new Date(listing.startDate.seconds * 1000);
        endDate = new Date(listing.endDate.seconds * 1000);

        switch (this.nearbyFilter.date) {
            case 'day':
                return (HelperService.getNow() > startDate) && (HelperService.getNow() < endDate);
            case 'week':
                let weekStartDate: Date = new Date(startDate);
                weekStartDate.setDate(startDate.getDate() - 7);
                return (HelperService.getNow() > weekStartDate) && (HelperService.getNow() < endDate);
            default:
                return HelperService.getNow() <= endDate;
        }
    }

    private calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
        let p = 0.017453292519943295;    // Math.PI / 180
        let c = Math.cos;
        let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        return (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    }
}