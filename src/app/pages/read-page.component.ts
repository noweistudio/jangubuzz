import {PageComponent} from './page.component';
import {ActivatedRoute, Params} from '@angular/router';
import {Observable} from 'rxjs/index';
import {CrudService} from '../services/crud.service';
import {take} from 'rxjs/internal/operators';

export abstract class ReadPageComponent extends PageComponent {
    public id: string;
    public model: any;
    public modelObservable: Observable<any>;

    constructor(public activatedRoute: ActivatedRoute,
                public crudService: CrudService) {
        super();

        this.activatedRoute.params.subscribe(
            (params: Params) => {
                this.id = params['id'];
            }
        );

        if (this.id) {
            this.modelObservable = this.crudService.read(this.id);
            this.modelObservable.pipe(take(1)).subscribe((res) => {
                this.model = res;

                this.modelObservableCallback();
            });
        }
    }

    protected modelObservableCallback(): void {
    }
}