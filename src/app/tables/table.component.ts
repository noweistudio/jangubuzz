import {EventEmitter, Input, Output} from "@angular/core";

export class TableComponent {
    @Input() columns: string[] = [];
    @Input() dataSource: Array<any> = [];
    @Input() length: number = 0;
    @Input() pageIndex: number = 0;
    @Input() pageSize: number = 10;
    @Input() activated: {
        rowSelect?: boolean
    } = {
        rowSelect: true
    };
    @Output() onRowSelect = new EventEmitter();
    @Output() onPageChange = new EventEmitter();
    @Output() onWarmActionClick = new EventEmitter();

    constructor() {
    }
}