import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from './modules/material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuth, AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireStorage} from 'angularfire2/storage';
import {UploadModule} from './modules/upload/upload.module';
import {PaymentModule} from './modules/payment/payment.module';
import {environment} from '../environments/environment';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {CreateAccountPageComponent} from './pages/create-account-page/create-account-page.component';
import {RecoverPasswordPageComponent} from './pages/recover-password-page/recover-password-page.component';
import {LoginFormComponent} from './forms/login-form/login-form.component';
import {CreateAccountFormComponent} from './forms/create-account-form/create-account-form.component';
import {RecoverPasswordFormComponent} from './forms/recover-password-form/recover-password-form.component';
import {TextLineBreakComponent} from './fragments/text-line-break/text-line-break.component';
import {BirthdateDialogComponent} from './dialogs/birthdate-dialog/birthdate-dialog.component';
import {ApiInterceptor} from './interceptors/api.interceptor';
import {MediaMatcher} from '@angular/cdk/layout';
import {LoadingService} from './services/loading.service';
import {SnackbarService} from './services/snackbar.service';
import {ApiService} from './services/api.service';
import {AuthService} from './services/auth.service';
import {UserService} from './services/user.service';
import 'hammerjs';

@NgModule({
    declarations: [
        AppComponent,
        LoginPageComponent,
        RecoverPasswordPageComponent,
        LoginFormComponent,
        RecoverPasswordFormComponent,
        TextLineBreakComponent,
        CreateAccountFormComponent,
        CreateAccountPageComponent,
        BirthdateDialogComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        MaterialModule,
        FlexLayoutModule,
        HttpClientModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        UploadModule,
        PaymentModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
        MediaMatcher,
        AngularFireAuth,
        AngularFirestore,
        AngularFireStorage,
        LoadingService,
        SnackbarService,
        ApiService,
        AuthService,
        UserService
    ],
    entryComponents: [
        BirthdateDialogComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}