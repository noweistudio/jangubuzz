import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UploadImageFormComponent} from '../../forms/upload-image-form/upload-image-form.component';
import {UploadDialogComponent} from '../../dialogs/upload-dialog/upload-dialog.component';
import {UploadService} from '../../services/upload.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
    ],
    exports: [
        UploadImageFormComponent
    ],
    declarations: [
        UploadImageFormComponent,
        UploadDialogComponent
    ],
    providers: [
        UploadService
    ],
    entryComponents: [
        UploadDialogComponent
    ]
})
export class UploadModule {
}
