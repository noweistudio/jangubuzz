import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component} from '@angular/core';
import {Router} from '@angular/router';
import {LoadingService} from '../../services/loading.service';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss']
})
export class PageComponent {
    public mobileQuery: MediaQueryList;
    private _mobileQueryListener: () => void;

    constructor(public changeDetectorRef: ChangeDetectorRef,
                public media: MediaMatcher,
                public router: Router,
                public loadingService: LoadingService,
                public authService: AuthService) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }
}
