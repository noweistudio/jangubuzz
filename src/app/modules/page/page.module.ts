import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageComponent} from './page.component';
import {PageRoutingModule} from './page-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {QRCodeModule} from 'angularx-qrcode';
import {UploadModule} from '../upload/upload.module';
import {AgmCoreModule} from '@agm/core';
import {ActivitiesPageComponent} from '../../pages/activities-page/activities-page.component';
import {DocumentsPageComponent} from '../../pages/documents-page/documents-page.component';
import {GuestsPageComponent} from '../../pages/guests-page/guests-page.component';
import {NearbyPageComponent} from '../../pages/nearby-page/nearby-page.component';
import {NotificationsPageComponent} from '../../pages/notifications-page/notifications-page.component';
import {ReceiptPageComponent} from '../../pages/receipt-page/receipt-page.component';
import {SettingsPageComponent} from '../../pages/settings-page/settings-page.component';
import {UserPageComponent} from '../../pages/user-page/user-page.component';
import {UserProfilePageComponent} from '../../pages/user-profile-page/user-profile-page.component';
import {BuzzBalancePageComponent} from '../../pages/buzz-balance-page/buzz-balance-page.component';
import {BuzzInvitePageComponent} from '../../pages/buzz-invite-page/buzz-invite-page.component';
import {EventPageComponent} from '../../pages/event-page/event-page.component';
import {EventEditPageComponent} from '../../pages/event-edit-page/event-edit-page.component';
import {MyEventsPageComponent} from '../../pages/my-events-page/my-events-page.component';
import {MyTicketsPageComponent} from '../../pages/my-tickets-page/my-tickets-page.component';
import {TeamPageComponent} from '../../pages/team-page/team-page.component';
import {TicketPurchasePageComponent} from '../../pages/ticket-purchase-page/ticket-purchase-page.component';
import {TicketTimelinePageComponent} from '../../pages/ticket-timeline-page/ticket-timeline-page.component';
import {UserAutoCompleteComponent} from '../../auto-completes/user-auto-complete/user-auto-complete.component';
import {BlockBottomSheetComponent} from '../../bottom-sheets/block-bottom-sheet/block-bottom-sheet.component';
import {EventBottomSheetComponent} from '../../bottom-sheets/event-bottom-sheet/event-bottom-sheet.component';
import {StandardBottomSheetComponent} from '../../bottom-sheets/standard-bottom-sheet/standard-bottom-sheet.component';
import {TicketBottomSheetComponent} from '../../bottom-sheets/ticket-bottom-sheet/ticket-bottom-sheet.component';
import {TicketsBottomSheetComponent} from '../../bottom-sheets/tickets-bottom-sheet/tickets-bottom-sheet.component';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import {QrCodeDialogComponent} from '../../dialogs/qr-code-dialog/qr-code-dialog.component';
import {TicketEditDialogComponent} from '../../dialogs/ticket-edit-dialog/ticket-edit-dialog.component';
import {CommentCardComponent} from '../../cards/comment-card/comment-card.component';
import {EventCardComponent} from '../../cards/event-card/event-card.component';
import {GuestsCardComponent} from '../../cards/guests-card/guests-card.component';
import {TicketCardComponent} from '../../cards/ticket-card/ticket-card.component';
import {TicketEditCardComponent} from '../../cards/ticket-edit-card/ticket-edit-card.component';
import {BuzzInviteFormComponent} from '../../forms/buzz-invite-form/buzz-invite-form.component';
import {UserFormComponent} from '../../forms/user-form/user-form.component';
import {UserEmailFormComponent} from '../../forms/user-email-form/user-email-form.component';
import {UserPasswordFormComponent} from '../../forms/user-password-form/user-password-form.component';
import {UserDeleteFormComponent} from '../../forms/user-delete-form/user-delete-form.component';
import {CommentFormComponent} from '../../forms/comment-form/comment-form.component';
import {NearbyFilterFormComponent} from '../../forms/nearby-filter-form/nearby-filter-form.component';
import {PromotionFormComponent} from '../../forms/promotion-form/promotion-form.component';
import {ReplyFormComponent} from '../../forms/reply-form/reply-form.component';
import {EventFormComponent} from '../../forms/event-form/event-form.component';
import {TicketFormComponent} from '../../forms/ticket-form/ticket-form.component';
import {TicketPurchaseFormComponent} from '../../forms/ticket-purchase-form/ticket-purchase-form.component';
import {VenueFormComponent} from '../../forms/venue-form/venue-form.component';
import {CommentListItemComponent} from '../../list-items/comment-list-item/comment-list-item.component';
import {CommentFormListItemComponent} from '../../list-items/comment-form-list-item/comment-form-list-item.component';
import {ReplyListItemComponent} from '../../list-items/reply-list-item/reply-list-item.component';
import {ReplyFormListItemComponent} from '../../list-items/reply-form-list-item/reply-form-list-item.component';
import {BuzzInvitationNotificationComponent} from '../../list-items/notifications/buzz-invitation-notification/buzz-invitation-notification.component';
import {EventWasCanceledNotificationComponent} from '../../list-items/notifications/event-was-canceled-notification/event-was-canceled-notification.component';
import {EventWasDeletedNotificationComponent} from '../../list-items/notifications/event-was-deleted-notification/event-was-deleted-notification.component';
import {EventWasOpenedNotificationComponent} from '../../list-items/notifications/event-was-opened-notification/event-was-opened-notification.component';
import {FollowingNotificationComponent} from '../../list-items/notifications/following-notification/following-notification.component';
import {RefundDeclinedNotificationComponent} from '../../list-items/notifications/refund-declined-notification/refund-declined-notification.component';
import {RefundGrantedNotificationComponent} from '../../list-items/notifications/refund-granted-notification/refund-granted-notification.component';
import {RefundRequestedNotificationComponent} from '../../list-items/notifications/refund-requested-notification/refund-requested-notification.component';
import {TeamEditNotificationComponent} from '../../list-items/notifications/team-edit-notification/team-edit-notification.component';
import {UserCommentedNotificationComponent} from '../../list-items/notifications/user-commented-notification/user-commented-notification.component';
import {UserJoinedEventNotificationComponent} from '../../list-items/notifications/user-joined-event-notification/user-joined-event-notification.component';
import {UserRepliedNotificationComponent} from '../../list-items/notifications/user-replied-notification/user-replied-notification.component';
import {TextCircleComponent} from '../../fragments/text-circle/text-circle.component';
import {ImagePreloadDirective} from '../../directives/image-preload.directive';
import {GooglePlacesDirective} from '../../directives/google-places.directive';
import {BuzzService} from '../../services/buzz.service';
import {ListingService} from '../../services/listing.service';
import {ListingBookmarkService} from '../../services/listing-bookmark.service';
import {ListingCommentService} from '../../services/listing-comment.service';
import {ListingCommentReplyService} from '../../services/listing-comment-reply.service';
import {ListingGoingService} from '../../services/listing-going.service';
import {ListingMoneyMadeService} from '../../services/listing-money-made.service';
import {ListingPurchasedTicketService} from '../../services/listing-purchased-ticket.service';
import {ListingTicketService} from '../../services/listing-ticket.service';
import {NotificationService} from '../../services/notification.service';
import {TeamService} from '../../services/team.service';
import {UserService} from '../../services/user.service';
import {UserBlockService} from '../../services/user-block.service';
import {UserBuzzSendService} from '../../services/user-buzz-send.service';
import {UserFollowerService} from '../../services/user-follower.service';
import {UserFollowingService} from '../../services/user-following.service';
import {UserListingBookmarkedService} from '../../services/user-listing-bookmarked.service';
import {UserListingGoingService} from '../../services/user-listing-going.service';
import {UserNearbyService} from '../../services/user-nearby.service';
import {UserNotificationService} from '../../services/user-notification.service';
import {UserReceiptService} from '../../services/user-receipt.service';
import {UserTicketService} from '../../services/user-ticket.service';

@NgModule({
    imports: [
        CommonModule,
        PageRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        QRCodeModule,
        UploadModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDURY1Pw5y-NxzrbNzp98hD_h0WWoKN8sI'
        })
    ],
    declarations: [
        PageComponent,
        ActivitiesPageComponent,
        DocumentsPageComponent,
        GuestsPageComponent,
        NearbyPageComponent,
        NotificationsPageComponent,
        ReceiptPageComponent,
        SettingsPageComponent,
        UserPageComponent,
        UserProfilePageComponent,
        BuzzBalancePageComponent,
        BuzzInvitePageComponent,
        EventPageComponent,
        EventEditPageComponent,
        MyEventsPageComponent,
        MyTicketsPageComponent,
        TeamPageComponent,
        TicketPurchasePageComponent,
        TicketTimelinePageComponent,
        UserAutoCompleteComponent,
        BlockBottomSheetComponent,
        EventBottomSheetComponent,
        StandardBottomSheetComponent,
        TicketBottomSheetComponent,
        TicketsBottomSheetComponent,
        ConfirmationDialogComponent,
        QrCodeDialogComponent,
        TicketEditDialogComponent,
        CommentCardComponent,
        EventCardComponent,
        GuestsCardComponent,
        TicketCardComponent,
        TicketEditCardComponent,
        BuzzInviteFormComponent,
        UserFormComponent,
        UserEmailFormComponent,
        UserPasswordFormComponent,
        UserDeleteFormComponent,
        CommentFormComponent,
        NearbyFilterFormComponent,
        PromotionFormComponent,
        EventFormComponent,
        ReplyFormComponent,
        TicketFormComponent,
        TicketPurchaseFormComponent,
        VenueFormComponent,
        CommentListItemComponent,
        CommentFormListItemComponent,
        ReplyListItemComponent,
        ReplyFormListItemComponent,
        BuzzInvitationNotificationComponent,
        EventWasCanceledNotificationComponent,
        EventWasDeletedNotificationComponent,
        EventWasOpenedNotificationComponent,
        FollowingNotificationComponent,
        RefundDeclinedNotificationComponent,
        RefundGrantedNotificationComponent,
        RefundRequestedNotificationComponent,
        TeamEditNotificationComponent,
        UserCommentedNotificationComponent,
        UserJoinedEventNotificationComponent,
        UserRepliedNotificationComponent,
        TextCircleComponent,
        ImagePreloadDirective,
        GooglePlacesDirective
    ],
    providers: [
        BuzzService,
        ListingService,
        ListingBookmarkService,
        ListingCommentService,
        ListingCommentReplyService,
        ListingGoingService,
        ListingMoneyMadeService,
        ListingPurchasedTicketService,
        ListingTicketService,
        NotificationService,
        TeamService,
        UserService,
        UserBlockService,
        UserBuzzSendService,
        UserFollowerService,
        UserFollowingService,
        UserListingBookmarkedService,
        UserListingGoingService,
        UserNearbyService,
        UserNotificationService,
        UserReceiptService,
        UserTicketService
    ],
    entryComponents: [
        BlockBottomSheetComponent,
        EventBottomSheetComponent,
        StandardBottomSheetComponent,
        TicketBottomSheetComponent,
        TicketsBottomSheetComponent,
        ConfirmationDialogComponent,
        QrCodeDialogComponent,
        TicketEditDialogComponent
    ]
})
export class PageModule {
}