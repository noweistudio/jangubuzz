import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageComponent} from './page.component';
import {ActivitiesPageComponent} from '../../pages/activities-page/activities-page.component';
import {BuzzBalancePageComponent} from '../../pages/buzz-balance-page/buzz-balance-page.component';
import {BuzzInvitePageComponent} from '../../pages/buzz-invite-page/buzz-invite-page.component';
import {DocumentsPageComponent} from '../../pages/documents-page/documents-page.component';
import {MyEventsPageComponent} from '../../pages/my-events-page/my-events-page.component';
import {MyTicketsPageComponent} from '../../pages/my-tickets-page/my-tickets-page.component';
import {TicketPurchasePageComponent} from '../../pages/ticket-purchase-page/ticket-purchase-page.component';
import {TicketTimelinePageComponent} from '../../pages/ticket-timeline-page/ticket-timeline-page.component';
import {EventPageComponent} from '../../pages/event-page/event-page.component';
import {EventEditPageComponent} from '../../pages/event-edit-page/event-edit-page.component';
import {TeamPageComponent} from '../../pages/team-page/team-page.component';
import {NearbyPageComponent} from '../../pages/nearby-page/nearby-page.component';
import {NotificationsPageComponent} from '../../pages/notifications-page/notifications-page.component';
import {ReceiptPageComponent} from '../../pages/receipt-page/receipt-page.component';
import {SettingsPageComponent} from '../../pages/settings-page/settings-page.component';
import {UserPageComponent} from '../../pages/user-page/user-page.component';
import {UserProfilePageComponent} from '../../pages/user-profile-page/user-profile-page.component';

const routes: Routes = [{
    path: '',
    component: PageComponent,
    children: [
        {path: '', redirectTo: 'nearby', pathMatch: 'full'},
        {path: 'activities', component: ActivitiesPageComponent},
        {path: 'buzz-balance', component: BuzzBalancePageComponent},
        {path: 'buzz-invite/:id', component: BuzzInvitePageComponent},
        {path: 'documents', component: DocumentsPageComponent},
        {path: 'events', component: MyEventsPageComponent},
        {path: 'event/:id', component: EventPageComponent},
        {path: 'event-edit', component: EventEditPageComponent},
        {path: 'event-edit/:id', component: EventEditPageComponent},
        {path: 'tickets', component: MyTicketsPageComponent},
        {path: 'ticket-purchase/:eventId/:id', component: TicketPurchasePageComponent},
        {path: 'ticket-timeline/:id', component: TicketTimelinePageComponent},
        {path: 'team', component: TeamPageComponent},
        {path: 'nearby', component: NearbyPageComponent},
        {path: 'notifications', component: NotificationsPageComponent},
        {path: 'receipt/:id', component: ReceiptPageComponent},
        {path: 'settings', component: SettingsPageComponent},
        {path: 'user', component: UserPageComponent},
        {path: 'user-profile/:id', component: UserProfilePageComponent}
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: []
})
export class PageRoutingModule {
}