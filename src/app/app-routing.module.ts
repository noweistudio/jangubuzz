import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AuthGuard} from "./guards/auth.guard";
import {UnAuthGuard} from "./guards/un-auth.guard";
import {LoginPageComponent} from "./pages/login-page/login-page.component";
import {CreateAccountPageComponent} from './pages/create-account-page/create-account-page.component';
import {RecoverPasswordPageComponent} from './pages/recover-password-page/recover-password-page.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'page',
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    {
        path: 'page',
        canActivate: [AuthGuard],
        loadChildren: './modules/page/page.module#PageModule'
    },
    {
        path: 'login',
        component: LoginPageComponent,
        canActivate: [UnAuthGuard]
    },
    {
        path: 'create-account',
        component: CreateAccountPageComponent,
        canActivate: [UnAuthGuard]
    },
    {
        path: 'recover-password',
        component: RecoverPasswordPageComponent,
        canActivate: [UnAuthGuard]
    },
    {
        path: '**',
        redirectTo: '/create-account',
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
        AuthGuard,
        UnAuthGuard
    ]
})
export class AppRoutingModule {
}