import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirthdateDialogComponent } from './birthdate-dialog.component';

describe('BirthdateDialogComponent', () => {
  let component: BirthdateDialogComponent;
  let fixture: ComponentFixture<BirthdateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthdateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
