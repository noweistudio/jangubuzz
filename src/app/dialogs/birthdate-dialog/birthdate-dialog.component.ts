import {Component, Inject} from '@angular/core';
import {DialogComponent} from '../dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-birthdate-dialog',
    templateUrl: './birthdate-dialog.component.html',
    styleUrls: ['./birthdate-dialog.component.scss']
})
export class BirthdateDialogComponent extends DialogComponent {
    public dialogTitle: string = 'Birth Date';

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<BirthdateDialogComponent>) {
        super(data);
    }
}
