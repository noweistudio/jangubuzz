import {Component, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogComponent} from '../dialog.component';

@Component({
    selector: 'app-upload-dialog',
    templateUrl: './upload-dialog.component.html',
    styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent extends DialogComponent {
    @ViewChild('file') file;

    public dialogTitle: string = 'Upload File';
    public files: Set<File> = new Set();

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<UploadDialogComponent>) {
        super(data);
    }

    public confirm(): void {
        this.dialogRef.close(this.files);
    }

    public addFiles(): void {
        this.file.nativeElement.click();
    }

    public onFilesAdded(): void {
        const files: { [key: string]: File } = this.file.nativeElement.files;
        for (let key in files) {
            if (!isNaN(parseInt(key))) {
                this.files.add(files[key]);
            }
        }
    }
}
