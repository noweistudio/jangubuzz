import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogComponent} from '../dialog.component';

@Component({
    selector: 'app-ticket-edit-dialog',
    templateUrl: './ticket-edit-dialog.component.html',
    styleUrls: ['./ticket-edit-dialog.component.scss']
})
export class TicketEditDialogComponent extends DialogComponent {
    public dialogTitle: string = 'Ticket';

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<TicketEditDialogComponent>) {
        super(data);
    }

    public confirm(): void {
        this.dialogRef.close(this.data.model);
    }
}
