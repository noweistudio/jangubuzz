import {Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export abstract class DialogComponent implements OnInit {
    public model: any = {};
    public dialogTitle: string = '';
    public dialogRef: MatDialogRef<any>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit(): void {
        this.model = this.data && this.data.model ? this.data.model : this.model;
        this.dialogTitle = this.data && this.data.dialogTitle ? this.data.dialogTitle : this.dialogTitle;
    }

    public close(): void {
        this.dialogRef.close();
    }
}