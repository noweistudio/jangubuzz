import {EventEmitter, Input, OnChanges, Output} from "@angular/core";
import {FormControl} from "@angular/forms";
import {ENTER} from "@angular/cdk/keycodes";

export abstract class ChipsInputComponent implements OnChanges {
    @Input() model: any[] = [];
    @Input() type: string = 'text';
    @Input() placeholder: string = '';
    @Input() max: number;
    @Output() modelChange = new EventEmitter();
    @Output() onChipAdd = new EventEmitter();
    @Output() onChipRemove = new EventEmitter();

    public inputFormCtrl: FormControl = new FormControl();

    public indexResponse: any;

    public separatorKeysCodes = [ENTER];

    constructor() {
    }

    ngOnChanges() {
        this.inputFormCtrl.setValue(this.model, {emitEvent: false});
        this.updateDisable();
    }

    /**
     * Update input disable property
     */
    protected updateDisable(): void {
        if (this.max && this.model.length >= this.max) {
            this.inputFormCtrl.disable({emitEvent: false});
        } else {
            this.inputFormCtrl.enable({emitEvent: false});
        }
    }
}