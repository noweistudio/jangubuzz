import {Injectable} from '@angular/core';

@Injectable()
export class LoadingService {
    /**
     * Loading array
     * @type {Array}
     * @private
     */
    private _loading: boolean[] = [];

    /**
     * If anything is loading
     */
    private _isLoading: boolean = false;

    constructor() {

    }

    /**
     * Get if anything is loading
     * @returns {boolean}
     */
    get isLoading() {
        return this._isLoading;
    }

    /**
     * Set a key as loading
     * @param key
     * @param value
     */
    public set(key: string, value: boolean) {
        this._loading[key] = value;
        this.checkLoading();

        setTimeout(()=> {
            this.delete(key);
        }, 10 * 1000);
    }

    /**
     * Get loading status by key
     * @param key
     * @returns {string}
     */
    public get(key: string) {
        return this._loading[key];
    }

    /**
     * Remove key from loading array
     * @param key
     */
    public delete(key: string) {
        delete this._loading[key];
        this.checkLoading();
    }

    /**
     * Clear loading array
     */
    public clear() {
        this._loading = [];
        this.checkLoading();
    }

    /**
     * Check if anything is loading
     * @returns {boolean}
     */
    public checkLoading(): boolean {
        let isLoading = false;

        Object.keys(this._loading).forEach((key) => {
            if (this._loading[key]) {
                isLoading = true;
            }
        });

        this._isLoading = isLoading;
        return isLoading;
    }
}