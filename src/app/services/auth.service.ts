import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {firebase} from '@firebase/app';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {User} from 'firebase';
import {BirthdateDialogComponent} from '../dialogs/birthdate-dialog/birthdate-dialog.component';
import {AngularFireAuth} from 'angularfire2/auth';
import {MatDialog} from '@angular/material';
import {SnackbarService} from './snackbar.service';
import {LoadingService} from './loading.service';
import {Router} from '@angular/router';
import {UserService} from './user.service';
import {Observable} from 'rxjs/index';
import {take} from 'rxjs/internal/operators';

@Injectable()
export class AuthService extends FirebaseService {
    public user: User;
    public userData: any;
    public userDataObservable: Observable<any>;
    public preLoading = true;

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public userService: UserService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public loadUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.afAuth.user.subscribe((res) => {
                this.user = res;
                this.loadUserData();
                this.preLoading = false;
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });
    }

    public loginWithEmail(payload: any): void {
        this.loadingService.set('login', true);
        this.afAuth.auth.signInWithEmailAndPassword(payload.email, payload.password).then((res) => {
            this.loadingService.delete('login');
            let date = new Date();
            this.afs.doc('users/' + this.afAuth.auth.currentUser.uid).update({
                lastLogin: date.toUTCString(),
                dateModified: date.toUTCString()
            });

            this.user = res.user;
            this.loadUserData();

            this.router.navigate(['page']);
            this.snackbarLogin();
        }).catch(() => {
            this.loadingService.delete('login');
            this.router.navigate(['login']);
            this.snackbarService.somethingWentWrong();
        });
    }

    public loginWithGoogle(): void {
        this.loadingService.set('login', true);
        this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then((res) => {
                this.loadingService.delete('login');
                const userDoc: AngularFirestoreDocument = this.afs.doc('users/' + this.afAuth.auth.currentUser.uid);

                if (res.additionalUserInfo.isNewUser) {
                    let date = new Date();
                    userDoc.set({
                        name: this.afAuth.auth.currentUser.displayName,
                        lastLogin: date.toUTCString(),
                        dateModified: date.toUTCString(),
                        dateCreated: date.toUTCString()
                    });

                    this.user = res.user;
                    this.loadUserData();

                    this.router.navigate(['page']);
                    this.snackbarLogin();

                    this.dialog.open(BirthdateDialogComponent, {
                        width: '250px',
                    }).afterClosed().subscribe((res) => {
                        userDoc.update({
                            birthDate: res ? res.birthDate : ''
                        });
                    });
                }
                else {
                    let date = new Date();
                    userDoc.update({
                        lastLogin: date.toUTCString(),
                        dateModified: date.toUTCString()
                    });

                    this.user = res.user;
                    this.router.navigate(['page']);
                    this.snackbarLogin();
                }
            }
        ).catch((err) => {
            this.loadingService.delete('login');
            this.router.navigate(['login']);
            this.snackbarService.somethingWentWrong();
        });
    }

    public loginAnonoymously(): void {
        this.loadingService.set('login', true);
        this.afAuth.auth.signInAnonymously().then((res) => {
            this.loadingService.delete('login');
            this.router.navigate(['page']);
        }).catch((err) => {
            this.loadingService.delete('login');
            this.router.navigate(['login']);
        });
    }

    public logout(): void {
        this.loadingService.set('logout', true);
        this.afAuth.auth.signOut().then((res) => {
            this.loadingService.delete('logout');
            this.router.navigate(['login']);
            this.snackbarLogout();
        }).catch(() => {
            this.loadingService.delete('logout');
            this.router.navigate(['login']);
        });
    }

    public createUserWithEmail(payload: any): void {
        this.loadingService.set('create-user', true);
        this.afAuth.auth.createUserWithEmailAndPassword(payload.email, payload.password).then((res) => {
            this.loadingService.delete('create-user');
            let date = new Date();
            this.afs.doc('users/' + this.afAuth.auth.currentUser.uid).set({
                name: payload.name,
                birthDate: payload.birthDate,
                lastLogin: date.toUTCString(),
                dateModified: date.toUTCString(),
                dateCreated: date.toUTCString()
            });

            this.afAuth.auth.currentUser.updateProfile({
                displayName: payload.name,
                photoURL: ''
            });
            this.loadUserData();

            this.router.navigate(['page']);
        }).catch((err) => {
            this.loadingService.delete('create-user');
            this.snackbarService.somethingWentWrong();
        });
    }

    public recoverPassword(payload: { email: string }): void {
        this.loadingService.set('recover-password', true);
        this.afAuth.auth.sendPasswordResetEmail(payload.email).then((res) => {
            this.loadingService.delete('recover-password');
            this.snackbarService.open('Recover password instruction has been sent to your email address.', 'Close');
            this.router.navigate(['login']);
        }).catch(() => {
            this.loadingService.delete('recover-password');
            this.snackbarService.somethingWentWrong();
        });
    }

    public updateEmail(newEmail: string, password: string): void {
        this.afAuth.auth.currentUser.reauthenticateAndRetrieveDataWithCredential(firebase.auth.EmailAuthProvider.credential(this.user.email, password)).then(() => {
            this.loadingService.set('update-email', true);
            this.afAuth.auth.currentUser.updateEmail(newEmail).then(() => {
                this.loadingService.delete('update-email');
                this.snackbarService.open('Your email has been updated.', 'Close');
            }).catch(() => {
                this.loadingService.delete('update-email');
                this.snackbarService.somethingWentWrong();
            });
        }).catch(() => {
            this.snackbarService.open('The credential entered was incorrect.', 'Close');
        });
    }

    public updatePassword(newPassword: string, password: string): void {
        this.afAuth.auth.currentUser.reauthenticateAndRetrieveDataWithCredential(firebase.auth.EmailAuthProvider.credential(this.user.email, password)).then(() => {
            this.loadingService.set('update-password', true);
            this.afAuth.auth.currentUser.updatePassword(newPassword).then(() => {
                this.loadingService.delete('update-password');
                this.snackbarService.open('Your password has been updated.', 'Close');
            }).catch(() => {
                this.loadingService.delete('update-password');
                this.snackbarService.somethingWentWrong();
            });
        }).catch(() => {
            this.snackbarService.open('The credential entered was incorrect.', 'Close');
        });
    }

    public deleteAcount(password: string): void {
        this.afAuth.auth.currentUser.reauthenticateAndRetrieveDataWithCredential(firebase.auth.EmailAuthProvider.credential(this.user.email, password)).then(() => {
            this.loadingService.set('delete-account', true);
            firebase.auth().currentUser.delete().then(() => {
                this.loadingService.delete('delete-account');
                this.snackbarService.open('Your account has been deleted.', 'Close');
                this.router.navigate(['login']);
            }).catch(() => {
                this.loadingService.delete('delete-account');
                this.snackbarService.somethingWentWrong();
            });
        }).catch(() => {
            this.snackbarService.open('The credential entered was incorrect.', 'Close');
        });
    }

    protected init(): void {
        this.loadUser();
    }

    private loadUserData(): void {
        if (!this.afAuth.auth.currentUser) {
            return;
        }
        this.userDataObservable = this.userService.read(this.afAuth.auth.currentUser.uid);
        this.userDataObservable.pipe(take(1)).subscribe((res) => {
            if (res && res.birthDate && res.birthDate.seconds) {
                res.birthDate = new Date(res.birthDate.seconds * 1000).toISOString();
            }
            this.userData = res;
        });
    }

    private snackbarLogin(): void {
        this.snackbarService.open('Welcome ' + (this.user.displayName || this.user.email) + '!', 'Close');
    }

    private snackbarLogout(): void {
        this.snackbarService.open('See you later!', 'Close');
    }
}