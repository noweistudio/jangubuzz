import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {MatDialog} from '@angular/material';
import {AngularFireDatabase} from 'angularfire2/database';
import {LoadingService} from './loading.service';
import {SnackbarService} from './snackbar.service';
import {AuthService} from './auth.service';
import {ListAttributeService} from './listing.service';
import {AngularFirestore} from 'angularfire2/firestore';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ListingMoneyMadeService extends ListAttributeService {
    protected attribute: string = 'moneyMade';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }
}