import { TestBed, inject } from '@angular/core/testing';

import { UserNotificationService } from './user-notification.service';

describe('UserNotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserNotificationService]
    });
  });

  it('should be created', inject([UserNotificationService], (service: UserNotificationService) => {
    expect(service).toBeTruthy();
  }));
});
