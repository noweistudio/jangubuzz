import { TestBed, inject } from '@angular/core/testing';

import { ListingCommentService } from './listing-comment.service';

describe('ListingCommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingCommentService]
    });
  });

  it('should be created', inject([ListingCommentService], (service: ListingCommentService) => {
    expect(service).toBeTruthy();
  }));
});
