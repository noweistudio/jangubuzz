import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {Observable} from 'rxjs/index';
import {AngularFirestoreCollection} from 'angularfire2/firestore';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class NotificationService extends FirebaseService {
    public notificationCollection: AngularFirestoreCollection<any>;

    public notificationObservable: Observable<any>;

    protected init(): void {
        this.notificationCollection = this.afs.collection('users/' + this.afAuth.auth.currentUser.uid + '/notifications', ref => ref.orderBy('dateCreated', 'desc'));

        this.notificationObservable = this.notificationCollection.valueChanges().pipe(
            tap((res) => {
                return res;
            }));
    }
}