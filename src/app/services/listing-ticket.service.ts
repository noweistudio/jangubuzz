import {Injectable} from '@angular/core';
import {SnackbarService} from './snackbar.service';
import {Router} from '@angular/router';
import {LoadingService} from './loading.service';
import {AngularFireDatabase} from 'angularfire2/database';
import {ListAttributeService} from './listing.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {AuthService} from './auth.service';
import {MatDialog} from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class ListingTicketService extends ListAttributeService {
    protected attribute: string = 'tickets';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }
}