import {Injectable} from '@angular/core';
import {LoadingService} from './loading.service';
import {MatDialog} from '@angular/material';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {SnackbarService} from './snackbar.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export abstract class FirebaseService {
    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog) {
        this.init();
    }

    protected init(): void {
    }
}
