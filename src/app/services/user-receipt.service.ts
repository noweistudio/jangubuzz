import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserReceiptService extends UserAttributeService {
    protected attribute: string = 'receipts';

    public readAsCurrentUser(payloadId: string): Observable<any> {
        return super.read(this.afAuth.auth.currentUser.uid, payloadId);
    }
}