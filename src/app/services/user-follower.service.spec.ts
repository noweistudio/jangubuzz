import { TestBed, inject } from '@angular/core/testing';

import { UserFollowerService } from './user-follower.service';

describe('UserFollowerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserFollowerService]
    });
  });

  it('should be created', inject([UserFollowerService], (service: UserFollowerService) => {
    expect(service).toBeTruthy();
  }));
});
