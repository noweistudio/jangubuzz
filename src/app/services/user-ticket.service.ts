import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AngularFireDatabase} from 'angularfire2/database';
import {SnackbarService} from './snackbar.service';
import {MatDialog} from '@angular/material';
import {LoadingService} from './loading.service';
import {AuthService} from './auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {UserAttributeService} from './user.service';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable({
    providedIn: 'root'
})
export class UserTicketService extends UserAttributeService {
    protected attribute: string = 'tickets';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public index(id: string = this.afAuth.auth.currentUser.uid): Observable<any> {
        this.loadingService.set('index-user-' + this.attribute, true);

        return this.afs.collection('users/' + id + '/' + this.attribute, ref => ref.orderBy('dateCreated', 'desc')).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-user-' + this.attribute, false);
        }));
    }
}