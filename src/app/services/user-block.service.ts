import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {map} from 'rxjs/internal/operators';
import {Observable} from 'rxjs/index';

@Injectable({
    providedIn: 'root'
})
export class UserBlockService extends UserAttributeService {
    protected attribute: string = 'blocks';

    public blockAsCurrentUser(id: string, payload: any = {}): Promise<any> {
        return this.create(this.afAuth.auth.currentUser.uid, id, payload).then(() => {
            this.snackbarService.open('This user has been blocked.', 'Close');
        });
    }

    public unblockAsCurrentUser(id: string): Promise<any> {
        return this.delete(this.afAuth.auth.currentUser.uid, id).then(() => {
            this.snackbarService.open('This user has been unblocked.', 'Close');
        });
    }

    public currentUserIsBlocking(id: string): Observable<boolean> {
        return this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/' + this.attribute + '/' + id).snapshotChanges().pipe(
            map(({payload}) => payload.exists));
    }
}
