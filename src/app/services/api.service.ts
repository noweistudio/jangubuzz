import {Injectable} from "@angular/core";
import {Observable} from 'rxjs/index';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable()
export class ApiService {
    constructor(public http: HttpClient) {
    }

    /**
     * GET method
     * @param url
     * @param params
     */
    public get(url: string, params?: any): Observable<any> {
        let options = {
            params: ApiService.clean(params)
        };
        return this.http.get(ApiService.formatURL(url), options);
    }

    /**
     * POST method
     * @param {string} url
     * @param payload
     * @param params
     * @returns {Observable<any>}
     */
    public post(url: string, payload?: any, params?: any): Observable<any> {
        let options = {
            params: ApiService.clean(params)
        };
        return this.http.post(ApiService.formatURL(url), payload, options);
    }

    /**
     * PUT method
     * @param {string} url
     * @param payload
     * @param params
     * @returns {Observable<any>}
     */
    public put(url: string, payload?: any, params?: any): Observable<any> {
        let options = {
            params: ApiService.clean(params)
        };
        return this.http.put(ApiService.formatURL(url), payload, options);
    }

    /**
     * PATCH method
     * @param {string} url
     * @param payload
     * @param params
     * @returns {Observable<any>}
     */
    public patch(url: string, payload?: any, params?: any): Observable<any> {
        let options = {
            params: ApiService.clean(params)
        };
        return this.http.patch(ApiService.formatURL(url), payload, options);
    }

    /**
     * DELETE method
     * @param {string} url
     * @param params
     * @returns {Observable<any>}
     */
    public delete(url: string, params?: any): Observable<any> {
        let options = {
            params: ApiService.clean(params)
        };
        return this.http.delete(ApiService.formatURL(url), options);
    }

    /**
     * Add server api URL to request url
     * @param url
     * @returns {string}
     */
    public static formatURL(url: string): string {
        return environment.apiUrl + url;
    }

    /**
     * Delete empty properties
     * @param obj
     */
    public static clean(obj: any): any {
        for (let propName in obj) {
            if (obj[propName] === null || obj[propName] === undefined) {
                delete obj[propName];
            }
        }

        return obj;
    }
}