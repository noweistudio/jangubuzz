import {Injectable} from '@angular/core';
import {ListAttributeService} from './listing.service';
import {MatDialog} from '@angular/material';
import {AngularFireAuth} from 'angularfire2/auth';
import {LoadingService} from './loading.service';
import {AuthService} from './auth.service';
import {SnackbarService} from './snackbar.service';
import {Router} from '@angular/router';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable({
    providedIn: 'root'
})
export class ListingBookmarkService extends ListAttributeService {
    protected attribute: string = 'bookmarks';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }


    public bookmarkAsCurrentUser(id: string): Promise<any> {
        return this.create(id, this.afAuth.auth.currentUser.uid, this.authService.userData);
    }

    public unbookmarkAsCurrentUser(id: string): Promise<any> {
        return this.delete(id, this.afAuth.auth.currentUser.uid);
    }
}
