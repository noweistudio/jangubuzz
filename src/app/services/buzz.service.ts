import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BuzzService extends FirebaseService {

    public indexAsCurrentUser(): Observable<any> {
        this.loadingService.set('index-buzzInvite', true);

        return this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/private/buzzInvite').snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-buzzInvite', false);
        }));
    }
}
