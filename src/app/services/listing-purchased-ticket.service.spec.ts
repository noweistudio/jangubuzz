import { TestBed, inject } from '@angular/core/testing';

import { ListingPurchasedTicketService } from './listing-purchased-ticket.service';

describe('ListingPurchasedTicketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingPurchasedTicketService]
    });
  });

  it('should be created', inject([ListingPurchasedTicketService], (service: ListingPurchasedTicketService) => {
    expect(service).toBeTruthy();
  }));
});
