import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root'
})
export class UserFollowingService extends UserAttributeService {
    protected attribute: string = 'following';

    public followAsCurrentUser(id: string, payload: any = {}): Promise<any> {
        return this.create(this.afAuth.auth.currentUser.uid, id, payload);
    }

    public unfollowAsCurrentUser(id: string): Promise<any> {
        return this.delete(this.afAuth.auth.currentUser.uid, id);
    }

    public currentUserIsFollowing(id: string): Observable<boolean> {
        return this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/' + this.attribute + '/' + id).snapshotChanges().pipe(
            map(({payload}) => payload.exists));
    }
}
