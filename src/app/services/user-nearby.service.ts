import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {AngularFireDatabase} from 'angularfire2/database';
import {MatDialog} from '@angular/material';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {LoadingService} from './loading.service';
import {SnackbarService} from './snackbar.service';
import {Router} from '@angular/router';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserNearbyService extends UserAttributeService {
    protected attribute: string = 'nearBy';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public apiService: ApiService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public deleteCurrentUserNearby(): Observable<any> {
        return this.apiService.post('clean_up_near_by_events_v0_1', {userId: this.afAuth.auth.currentUser.uid});
    }

    public updateCurrentUserNearby(longitude: number, latitude: number): Observable<any> {
        return this.apiService.post('near_by_events_v0_1', {
            userId: this.afAuth.auth.currentUser.uid,
            lat: latitude,
            long: longitude
        });
    }
}