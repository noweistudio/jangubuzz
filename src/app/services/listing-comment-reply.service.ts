import {Injectable} from '@angular/core';
import {ListingCommentAttributeService} from './listing-comment.service';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ListingCommentReplyService extends ListingCommentAttributeService {
    protected attribute: string = 'replies';

    public index(eventId: string, commentId: string): Observable<any> {
        this.loadingService.set('index-listing-comment-' + this.attribute + '-' + commentId, true);

        return this.afs.collection('listings/' + eventId + '/comments/' + commentId + '/' + this.attribute, ref => ref.orderBy('dateCreated', 'desc')).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-listing-comment-' + this.attribute + '-' + commentId, false);
        }));
    }

    public create(eventId: string, commentId: string, payload: any = {}): Promise<any> {
        return super.create(eventId, commentId, this.afs.createId(), payload);
    }
}