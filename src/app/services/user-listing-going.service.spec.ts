import { TestBed, inject } from '@angular/core/testing';

import { UserListingGoingService } from './user-listing-going.service';

describe('UserListingGoingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserListingGoingService]
    });
  });

  it('should be created', inject([UserListingGoingService], (service: UserListingGoingService) => {
    expect(service).toBeTruthy();
  }));
});
