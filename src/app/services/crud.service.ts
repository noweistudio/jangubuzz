import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {HelperService} from './helper.service';

@Injectable()
export abstract class CrudService extends FirebaseService {
    protected model: string = '';

    public index(): Observable<any> {
        this.loadingService.set('index-' + this.model, true);

        return this.afs.collection(this.model).snapshotChanges().pipe(tap(() => {
            this.loadingService.delete('index-' + this.model);
        }));
    }

    public create(id: string = this.afs.createId(), payload: any = {}): Promise<any> {
        id = id || this.afs.createId();
        payload.userId = this.afAuth.auth.currentUser.uid;
        payload.dateCreated = HelperService.getDateCreated();
        payload.dateModified = HelperService.getDateModified();

        this.loadingService.set('create-' + this.model + '-' + id, true);

        return this.afs.doc(this.model + '/' + id).set(payload).then((res) => {
            this.loadingService.delete('create-' + this.model + '-' + id);
            return res;
        });
    }

    public read(id: string): Observable<any> {
        this.loadingService.set('read-' + this.model + '-' + id, true);

        return this.afs.doc(this.model + '/' + id).valueChanges().pipe(tap((res) => {
            if (res) {
                res._dateCreated = HelperService.convertDateToString(res.dateCreated);
                res._dateModified = HelperService.convertDateToString(res.dateModified);
            }

            this.loadingService.delete('read-' + this.model + '-' + id);
        }));
    }

    public update(id: string, payload: any): Promise<any> {
        this.loadingService.set('update-' + this.model + '-' + id, true);

        return this.afs.doc(this.model + '/' + id).update(payload).then(() => {
            this.loadingService.delete('update-' + this.model + '-' + id);
        }).catch(() => {
            this.loadingService.delete('update-' + this.model + '-' + id);
        });
    }

    public delete(id: string): Promise<any> {
        return this.afs.doc(this.model + '/' + id).delete();
    }

    public createId(): string {
        return this.afs.createId();
    }
}
