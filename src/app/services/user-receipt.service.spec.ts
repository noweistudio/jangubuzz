import { TestBed, inject } from '@angular/core/testing';

import { UserReceiptService } from './user-receipt.service';

describe('UserReceiptService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserReceiptService]
    });
  });

  it('should be created', inject([UserReceiptService], (service: UserReceiptService) => {
    expect(service).toBeTruthy();
  }));
});
