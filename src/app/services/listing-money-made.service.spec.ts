import { TestBed, inject } from '@angular/core/testing';

import { ListingMoneyMadeService } from './listing-money-made.service';

describe('ListingMoneyMadeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingMoneyMadeService]
    });
  });

  it('should be created', inject([ListingMoneyMadeService], (service: ListingMoneyMadeService) => {
    expect(service).toBeTruthy();
  }));
});
