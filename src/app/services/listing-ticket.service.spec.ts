import { TestBed, inject } from '@angular/core/testing';

import { ListingTicketService } from './listing-ticket.service';

describe('ListingTicketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingTicketService]
    });
  });

  it('should be created', inject([ListingTicketService], (service: ListingTicketService) => {
    expect(service).toBeTruthy();
  }));
});
