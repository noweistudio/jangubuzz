import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {Router} from '@angular/router';
import {SnackbarService} from './snackbar.service';
import {MatDialog} from '@angular/material';
import {AngularFireAuth} from 'angularfire2/auth';
import {LoadingService} from './loading.service';
import {AuthService} from './auth.service';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable({
    providedIn: 'root'
})
export class UserFollowerService extends UserAttributeService {
    protected attribute: string = 'followers';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public followAsCurrentUser(id: string): Promise<any> {
        return this.create(id, this.afAuth.auth.currentUser.uid, this.authService.userData);
    }

    public unfollowAsCurrentUser(id: string): Promise<any> {
        return this.delete(id, this.afAuth.auth.currentUser.uid);
    }
}
