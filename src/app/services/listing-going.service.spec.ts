import { TestBed, inject } from '@angular/core/testing';

import { ListingGoingService } from './listing-going.service';

describe('ListingGoingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingGoingService]
    });
  });

  it('should be created', inject([ListingGoingService], (service: ListingGoingService) => {
    expect(service).toBeTruthy();
  }));
});
