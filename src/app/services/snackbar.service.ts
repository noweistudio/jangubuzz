import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Injectable()
export class SnackbarService {
    constructor(public snackBar: MatSnackBar) {
    }

    public open(message: string, action: string = '', config: MatSnackBarConfig = {duration: 3000}): void {
        this.snackBar.open(message, action, config);
    }

    public somethingWentWrong(): void {
        this.open('Something went wrong.', 'Close', {duration: 5000});
    }

    /**
     * Show a toast message with error message
     * @param {object} error
     */
    public handleError(error: any) {
        let msg: string = '';
        let config: MatSnackBarConfig = new MatSnackBarConfig();
        config.duration = 3000;
        config.panelClass = 'warn';

        if (typeof error.errors === 'object') {
            for (let key in error.errors) {
                if (Array.isArray(error.errors[key])) {
                    msg = msg.concat('• ' + error.errors[key][0]) + '\n';
                } else {
                    msg = msg.concat('• ' + error.errors[key]) + '\n';
                }
            }
        } else {
            msg = msg.concat(error.message ? error.message : 'An error occurred');
        }

        this.snackBar.open(msg, 'Close', config);
    }
}
