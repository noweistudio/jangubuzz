import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {MatDialog} from '@angular/material';
import {AngularFireDatabase} from 'angularfire2/database';
import {LoadingService} from './loading.service';
import {SnackbarService} from './snackbar.service';
import {AuthService} from './auth.service';
import {AngularFirestore} from 'angularfire2/firestore';
import {UserAttributeService} from './user.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserBuzzSendService extends UserAttributeService {
    protected attribute: string = 'buzzSend';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public sendBuzzInviteAsCurrentUser(id: string, payload: any): Promise<any> {
        return this.create(this.afAuth.auth.currentUser.uid, id, payload).then(() => {
            this.snackbarService.open('Buzz Invite has been sent.', 'Close');
        }).catch(() => {
            this.snackbarService.somethingWentWrong();
        });
    }
}
