import { TestBed, inject } from '@angular/core/testing';

import { UserTicketService } from './user-ticket.service';

describe('UserTicketService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserTicketService]
    });
  });

  it('should be created', inject([UserTicketService], (service: UserTicketService) => {
    expect(service).toBeTruthy();
  }));
});
