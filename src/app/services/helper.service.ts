import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HelperService {
    constructor() {
    }

    public static getCategoryList(): any[] {
        return CATEGORY_LIST;
    }

    public static getMoodList(): any[] {
        return MOOD_LIST;
    }

    public static getHeatLevelThresholds(): any {
        return HEAT_LEVEL_THRESHOLDS;
    }

    public static getHeatLevelColors(): any {
        return HEAT_LEVEL_COLORS;
    }

    public static getHeatLevelWeights(): any {
        return HEAT_WEIGHTS;
    }

    public static getDayUnits(): any {
        return DAY_UNITS;
    }

    public static getNow(): any {
        return NOW;
    }

    public static getToday(): any {
        return NOW.toISOString();
    }

    public static getDefaultEvent(): any {
        return Object.assign({}, DEFAULT_EVENT);
    };

    public static getDefaultTicket(): any {
        return Object.assign({}, DEFAULT_TICKET);
    }

    public static getDefaultBuzzInvite(): any {
        return Object.assign({}, DEFAULT_BUZZ_INVITE);
    }

    public static getDefaultTicketRefundRequest(): any {
        return Object.assign({}, DEFAULT_TICKET_REFUND_REQUEST);
    }

    public static getDateCreated(): any {
        return {
            seconds: Math.round(new Date().getTime() / 1000)
        };
    }

    public static getDateModified(): any {
        return {
            seconds: Math.round(new Date().getTime() / 1000)
        };
    }

    public static convertDateToString(date: any): string {
        if (!date || !date.seconds) {
            return '';
        }
        return new Date(date.seconds * 1000).toISOString();
    }

    public static convertDateToObject(date: string): any {
        if (!date) {
            return {
                seconds: 0
            };
        }

        return {
            seconds: Math.round(new Date(date).getTime() / 1000)
        };
    }
}

const CATEGORY_LIST = [{
    text: 'Adventure',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fadventure%20(edited).jpg?alt=media&token=37ee63f4-74fb-4de1-96fa-769db0602931'
}, {
    text: 'Business',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fbusiness%20(edited).jpg?alt=media&token=3cb4c117-063a-413a-83bb-713756d94d34'
}, {
    text: 'Charity',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FCHARITY.jpg?alt=media&token=ecb2fccc-9edb-4f8a-8770-71ce22e29a98'
}, {
    text: 'Concert',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fconcert%20(edited).jpg?alt=media&token=4b603d62-36ac-44ef-b93c-7756fc103746'
}, {
    text: 'Conferences',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fconferences%20(edited).jpg?alt=media&token=98855e36-45c4-4f9c-a486-1b3ae426651a'
}, {
    text: 'Educative',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Feducation%20(edited).jpg?alt=media&token=65ab2cc6-2cbb-46cd-98ff-7d86efb463c1'
}, {
    text: 'Exhibition',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fexhibition%20use%20(edited).jpg?alt=media&token=84893aa8-0dbb-458e-adf6-3d6519e7707d'
}, {
    text: 'Fashion',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffashion%20(edited).jpg?alt=media&token=2ab9d22c-29b1-4846-86f0-3d92c894bfe5'
}, {
    text: 'Festival',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffestival%20(edited).jpg?alt=media&token=74f6e4c3-82aa-414d-a273-e6ad11926289'
}, {
    text: 'Film & Media',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffilm%20(edited).jpg?alt=media&token=bef56d2d-ecd4-45f0-88b1-4e560b008dc8'
}, {
    text: 'Food & Drinks',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffood%20(edited).jpg?alt=media&token=33e2e816-b039-4b68-a46f-96c8706172b3'
}, {
    text: 'Fun',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FFun.jpg?alt=media&token=cec95ddb-0921-4397-99c0-8cf3ed47ec22'
}, {
    text: 'Fundraisers',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffundraiser%20(edited).jpg?alt=media&token=db743bcf-757a-42da-9372-884445614c67'
}, {
    text: 'Hangout',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FHangout.jpg?alt=media&token=afe0ca03-6e19-44eb-accc-70f300435804'
}, {
    text: 'Health',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fhealth%20(edited).jpg?alt=media&token=f0c4540e-583c-4242-91c2-7841d762eedc'
}, {
    text: 'Networking',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FNETWORK%20(EDITED).jpg?alt=media&token=b554b7aa-2d63-476e-b261-73278c148e0a'
}, {
    text: 'Night Life',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fnight_life.png?alt=media&token=e530a36b-ec74-4cc4-a377-d21fffa027d8'
}, {
    text: 'Outdoor',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FOutdoor.jpg?alt=media&token=e8e2c231-80ee-4f3c-9294-9288bace83ce'
}, {
    text: 'Party',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fparty%20(edited).jpg?alt=media&token=91061b6a-33e8-4356-bef6-eafbb6c0831c'
}, {
    text: 'Performing Arts',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FPERFORMANCE%20ARTS%20(EDITED).jpg?alt=media&token=eb7f45a9-1e20-4804-b33d-1a26816a300c'
}, {
    text: 'Picnic',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2F-picnic%20(edited).jpg?alt=media&token=8de4ce8a-bf3f-4baa-a6bd-ea9cc873118f'
}, {
    text: 'Politics',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fpolitics%20(edited).jpg?alt=media&token=aa307c64-3595-44f7-9664-1d929124b45e'
}, {
    text: 'Religious',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Freligious%20(edited).jpg?alt=media&token=e7a5f1cb-4eb9-4236-b39c-b0138da507e1'
}, {
    text: 'Sports & Fitness',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Ffitness%20(edited).jpg?alt=media&token=489de913-9ba1-4123-94cb-a307fd26eda0'
}, {
    text: 'Tech & Science',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2Fscience%20(edited).jpg?alt=media&token=f7e2fb74-338b-405a-8780-330a97f32126'
}];

const MOOD_LIST = [{
    text: 'Adventurous',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FADVENTURE%203%20(edited).jpg?alt=media&token=18568a87-6973-4903-aa5b-fbc2674762e2'
}, {
    text: 'Angry',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FANGRY%20(edited).jpg?alt=media&token=85ac2e03-d0d4-43d9-aa22-5224870dd673'
}, {
    text: 'Anxious',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FANXIOUS%20(edited).jpg?alt=media&token=97a51732-cd40-4825-a8a3-6f809d94f4eb'
}, {
    text: 'Bored',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FBORED%20(edited).jpg?alt=media&token=d26297f9-8331-4fe2-8917-2976a7a199c4'
}, {
    text: 'Charitable',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FCHARITABLE%20(EDITED).jpg?alt=media&token=f4f5f981-7280-47c5-ab69-fbd3c0aea99c'
}, {
    text: 'Curious',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FCURIOUS%20(edited).jpg?alt=media&token=5b0bd81c-12b5-4bde-b041-cbb763db0bc4'
}, {
    text: 'Energetic',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FENERGETIC%20(edited).jpg?alt=media&token=1721fa4f-9abe-464c-a1f9-334307c13212'
}, {
    text: 'Grateful',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FGRATEFUL%20(edited).jpg?alt=media&token=f88e77c9-c6b4-459c-9ab6-92c0ae6bd5fe'
}, {
    text: 'Guilty',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FGUILT.jpg?alt=media&token=8d6a25ac-f0d2-49f0-b55e-7900c5f78f24'
}, {
    text: 'Happy',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FHAPPY%20(edited).jpg?alt=media&token=24df2c61-4fa4-4746-bc20-2c4b17b2c2a3'
}, {
    text: 'Inspired',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/categories%2FINSIPRED%20(edited)%20copy.jpg?alt=media&token=223b4ed3-9b33-4975-a3fe-ace46fe896b5'
}, {
    text: 'Jealous',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FJEALOUS%20(edited).jpg?alt=media&token=ade2ee12-27ca-4766-9fbd-883abe59029a'
}, {
    text: 'Lazy',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FLAZY%20(edited).jpg?alt=media&token=e22e66dc-4669-4260-983b-3db12f6f09e2'
}, {
    text: 'Lit',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FLIT%20(edited).jpg?alt=media&token=59ae099b-d8ac-4d3c-b5cf-f4621707c74a'
}, {
    text: 'Lonely',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FLONELY%20(edited).jpg?alt=media&token=c72dbc19-f07c-4f8d-8631-efb3c24b9dba'
}, {
    text: 'Relaxed',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FRELAXED%20(edited).jpg?alt=media&token=c54c26dd-f50f-445c-b7b2-1a333c2cc195'
}, {
    text: 'Religious',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FRELIGION%20(edited).jpg?alt=media&token=f75f304a-674d-44a1-8b0b-123d0bde871a'
}, {
    text: 'Sad',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FSAD_DEPRESSED1%20(edited).jpg?alt=media&token=0b8ccff0-d866-4ce1-b437-f63468bc9c21'
}, {
    text: 'Stressed',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FSTRESSED%20(edited).jpg?alt=media&token=1f140636-5998-4ff4-8544-de9b22833924'
}, {
    text: 'Surprised',
    url: 'https://firebasestorage.googleapis.com/v0/b/jangubuzz-e67f1.appspot.com/o/moods%2FSURPRISED%20(edited).jpg?alt=media&token=9b3832c8-3db3-42ae-9387-790f27d14b61'
}];

const HEAT_LEVEL_THRESHOLDS: any = {
    I: 33,
    II: 66,
    III: 99,
    MAX: 100
};

const HEAT_LEVEL_COLORS: any = {
    ZERO: 'gray',
    I: 'yellow',
    II: 'orange',
    III: 'red'
};

const HEAT_WEIGHTS: any = {
    VIEW: 1,
    BOOKMARK: 2,
    COMMENT: 3,
    REPLY: 3,
    GOING: 4,
    TICKET: 6
};

const DAY_UNITS: any = {
    MILLISECONDS: 24 * 60 * 60 * 1000
};

const NOW: Date = new Date();

const DEFAULT_EVENT = {
    userId: '',
    profileImageUrl: '',
    smallProfileImageUrl: '',
    imageOrientation: 0, // todo here 0 or 1
    name: '',
    startDate: '',
    endDate: '',
    age: 0,
    categories: {},
    description: '',
    venueName: '',
    venueAddress: '',
    geoPoint: null,
    priceArray: [],
    isFree: true,
    locale: '',
    listingType: 'event',
    bonus: true,
    isCanceled: false,
    bookmarksCount: 0,
    goingCount: 0,
    viewsCount: 0,
    commentsCount: 0,
    repliesCount: 0,
    ticketsCount: 0,
    dateCreated: null,
    dateModified: null
};

const DEFAULT_TICKET = {
    title: '',
    price: 0,
    amount: 1,
    maxNumber: 1,
    info: '',
    refundable: true,
    forShow: true,
    locale: '',
    dateCreated: '',
    dateModified: ''
};

const DEFAULT_BUZZ_INVITE = {
    listingId: '',
    eventName: '',
    eventPic: '',
    eventPrice: 0,
    eventStartDate: null,
    eventCategories: [],
    eventMoods: [],
    hostId: '',
    hostName: '',
    hostPic: '',
    locale: '',
    latitude: 0,
    longitude: 0,
    buzzRadius: 1,
    useBonus: true
};

const DEFAULT_TICKET_REFUND_REQUEST = {
    type : 'ticketRefundRequest',
    userId: '',
    ticketId : '',
    listingId : ''
};
