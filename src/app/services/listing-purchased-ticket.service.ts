import {Injectable} from '@angular/core';
import {ListAttributeService} from './listing.service';

@Injectable({
    providedIn: 'root'
})
export class ListingPurchasedTicketService extends ListAttributeService {
    protected attribute: string = 'purchasedTickets';
}