import {Injectable} from '@angular/core';
import {UserAttributeService} from './user.service';
import {Router} from '@angular/router';
import {SnackbarService} from './snackbar.service';
import {MatDialog} from '@angular/material';
import {AngularFireAuth} from 'angularfire2/auth';
import {LoadingService} from './loading.service';
import {AuthService} from './auth.service';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs/index';
import {map} from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root'
})
export class UserListingBookmarkedService extends UserAttributeService {
    protected attribute: string = 'bookmarks';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public index(id: string = this.afAuth.auth.currentUser.uid): Observable<any> {
        this.loadingService.set('index-user-' + this.attribute, true);

        return this.afs.collection('users/' + id + '/' + this.attribute, ref => ref.orderBy('endDate', 'desc')).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-user-' + this.attribute, false);
        }));
    }

    public bookmarkAsCurrentUser(id: string,  payload: any = {}): Promise<any> {
        return this.create(this.afAuth.auth.currentUser.uid, id, payload);
    }

    public unbookmarkAsCurrentUser(id: string): Promise<any> {
        return this.delete(this.afAuth.auth.currentUser.uid, id);
    }

    public currentUserIsBookmarked(id: string): Observable<boolean> {
        return this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/' + this.attribute + '/' + id).snapshotChanges().pipe(
            map(({payload}) => payload.exists));
    }
}
