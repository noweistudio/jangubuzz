import { TestBed, inject } from '@angular/core/testing';

import { UserListingBookmarkedService } from './user-listing-bookmarked.service';

describe('UserListingBookmarkedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserListingBookmarkedService]
    });
  });

  it('should be created', inject([UserListingBookmarkedService], (service: UserListingBookmarkedService) => {
    expect(service).toBeTruthy();
  }));
});
