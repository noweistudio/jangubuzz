import { TestBed, inject } from '@angular/core/testing';

import { ListingBookmarkService } from './listing-bookmark.service';

describe('ListingBookmarkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingBookmarkService]
    });
  });

  it('should be created', inject([ListingBookmarkService], (service: ListingBookmarkService) => {
    expect(service).toBeTruthy();
  }));
});
