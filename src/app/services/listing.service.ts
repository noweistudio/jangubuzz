import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs/index';
import {tap} from 'rxjs/operators';
import {map} from 'rxjs/internal/operators';
import {CrudService} from './crud.service';
import {HelperService} from './helper.service';
import {AngularFireDatabase} from 'angularfire2/database';
import {MatDialog} from '@angular/material';
import {SnackbarService} from './snackbar.service';
import {LoadingService} from './loading.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireStorage} from 'angularfire2/storage';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class ListingService extends CrudService {
    public listingsCollection: AngularFirestoreCollection<any>;
    public listingsObservable: Observable<any>;

    protected model: string = 'listings';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public store: AngularFireStorage) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    protected init(): void {
        this.listingsCollection = this.afs.collection('listings');
    }

    public create(id: string = this.afs.createId(), payload: any = {}): Promise<any> {
        payload = this.formatPayload(payload);

        return super.create(id, payload);
    }

    public read(id: string): Observable<any> {
        return super.read(id).pipe(map((res: any) => {
            if (res) {
                res._categories = Object.keys(res.categories);
                res._startDate = HelperService.convertDateToString(res.startDate);
                res._endDate = HelperService.convertDateToString(res.endDate);

                let addressArray: string[] = res.venueAddress.split(/-(.+)/);

                if (addressArray.length >= 2) {
                    res._unit = addressArray[0];
                    res._venueAddress = addressArray[1];
                }
            }

            return res;
        }));
    }

    public update(id: string, payload: any): Promise<any> {
        payload = this.formatPayload(payload);

        return super.update(id, payload);
    }

    public indexUserHosting(id: string): Observable<any> {
        this.loadingService.set('index-user-listings-' + id, true);

        return this.afs.collection('listings', ref => ref.where('userId', '==', id).orderBy('dateCreated', 'desc')).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-user-listings-' + id, false);
        }));
    }

    public indexCurrentUserHosting(): Observable<any> {
        return this.indexUserHosting(this.afAuth.auth.currentUser.uid);
    }

    public createListingImage(id: string, payload: any): Promise<any> {
        this.loadingService.set('create-' + this.model + '-image-' + id, true);

        return Promise.all([
            this.store.storage.ref().child('listings').child(this.afAuth.auth.currentUser.uid).child(id).child('image.jpg').put(payload),
            this.store.storage.ref().child('listings').child(this.afAuth.auth.currentUser.uid).child(id).child('smallImage.jpg').put(payload)
        ]).then((res) => {
            this.loadingService.delete('create-' + this.model + '-image-' + id);
            return res;
        });
    }

    private formatPayload(payload: any): any {
        payload.priceArray = _.sortBy(payload.priceArray, [(v) => {
            return v;
        }]);

        payload.isFree = payload.priceArray && payload.priceArray.length === 0;
        payload.startDate = HelperService.convertDateToObject(payload._startDate);
        payload.endDate = HelperService.convertDateToObject(payload._endDate);

        return payload;
    }
}

export abstract class ListAttributeService extends FirebaseService {
    protected attribute: string = '';

    public index(id: string): Observable<any> {
        this.loadingService.set('index-listing-' + this.attribute + '-' + id, true);

        return this.afs.collection('listings/' + id + '/' + this.attribute).snapshotChanges().pipe(tap(() => {
            this.loadingService.delete('index-listing-' + this.attribute + '-' + id);
        }));
    }

    public create(targetId: string, payloadId: string = this.afs.createId(), payload: any = {}): Promise<any> {
        payload.dateCreated = HelperService.getDateCreated();
        payload.dateModified = HelperService.getDateModified();

        return this.afs.doc('listings/' + targetId + '/' + this.attribute + '/' + payloadId).set(payload);
    }

    public read(targetId: string, payloadId: string): Observable<any> {
        this.loadingService.set('read-listing-' + this.attribute + '-' + payloadId, true);

        return this.afs.doc('listings/' + targetId + '/' + this.attribute + '/' + payloadId).valueChanges().pipe(tap((res) => {
            this.loadingService.delete('read-listing-' + this.attribute + '-' + payloadId);
        }));
    }

    public update(targetId: string, payloadId: string = this.afs.createId(), payload: any = {}): Promise<any> {
        this.loadingService.set('update-listing-' + this.attribute + '-' + payloadId, true);

        return this.afs.doc('listings/' + targetId + '/' + this.attribute + '/' + payloadId).update(payload).then(() => {
            this.loadingService.delete('update-listing-' + this.attribute + '-' + payloadId);
        }).catch(() => {
            this.loadingService.delete('update-listing-' + this.attribute + '-' + payloadId);
        });
    }

    public delete(targetId: string, payloadId: string): Promise<any> {
        return this.afs.doc('listings/' + targetId + '/' + this.attribute + '/' + payloadId).delete();
    }

    public createId(): string {
        return this.afs.createId();
    }
}