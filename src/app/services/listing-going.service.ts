import {Injectable} from '@angular/core';
import {ListAttributeService} from './listing.service';
import {Router} from '@angular/router';
import {AngularFireDatabase} from 'angularfire2/database';
import {SnackbarService} from './snackbar.service';
import {MatDialog} from '@angular/material';
import {LoadingService} from './loading.service';
import {AuthService} from './auth.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable({
    providedIn: 'root'
})
export class ListingGoingService extends ListAttributeService {
    protected attribute: string = 'going';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    public joinAsCurrentUser(id: string): Promise<any> {
        return this.create(id, this.afAuth.auth.currentUser.uid, this.authService.userData);
    }

    public unjoinAsCurrentUser(id: string): Promise<any> {
        return this.delete(id, this.afAuth.auth.currentUser.uid);
    }
}