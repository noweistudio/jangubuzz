import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs/index';
import {tap} from 'rxjs/operators';
import {CrudService} from './crud.service';
import {HelperService} from './helper.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireStorage} from 'angularfire2/storage';
import {MatDialog} from '@angular/material';
import {LoadingService} from './loading.service';
import {SnackbarService} from './snackbar.service';
import {AngularFireDatabase} from 'angularfire2/database';

@Injectable({
    providedIn: 'root'
})
export class UserService extends CrudService {
    public usersCollection: AngularFirestoreCollection<any>;
    public usersObservable: Observable<any>;

    protected model: string = 'users';

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public store: AngularFireStorage) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    protected init(): void {
        this.usersCollection = this.afs.collection('users');
    }

    // todo change this to take no param
    public index(text: string = ''): Observable<any> {
        this.loadingService.set('index-users', true);

        return this.afs.collection('users', ref => ref.where('name', '>=', text)).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-users', false);
        }));
    }

    public updateCurrentUser(payload: any): Promise<any> {
        return this.update(this.afAuth.auth.currentUser.uid, payload);
    }

    public updateCurrentUserImage(payload: any): Promise<any> {
        this.loadingService.set('create-' + this.model + '-image-' + this.afAuth.auth.currentUser.uid, true);

        return Promise.all([
            this.store.storage.ref().child('users').child(this.afAuth.auth.currentUser.uid).child(this.afAuth.auth.currentUser.uid).child('image.jpg').put(payload),
            this.store.storage.ref().child('users').child(this.afAuth.auth.currentUser.uid).child(this.afAuth.auth.currentUser.uid).child('smallImage.jpg').put(payload)
        ]).then((res) => {
            this.loadingService.delete('create-' + this.model + '-image-' + this.afAuth.auth.currentUser.uid);
            return res;
        });
    }
}

export abstract class UserAttributeService extends FirebaseService {
    protected attribute: string = '';

    public index(id: string = this.afAuth.auth.currentUser.uid): Observable<any> {
        this.loadingService.set('index-user-' + this.attribute + '-' + id, true);

        return this.afs.collection('users/' + id + '/' + this.attribute).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-user-' + this.attribute + '-' + id, false);
        }));
    }

    public create(targetId: string, payloadId: string, payload: any = {}): Promise<any> {
        payload.dateCreated = HelperService.getDateCreated();
        payload.dateModified = HelperService.getDateModified();

        return this.afs.doc('users/' + targetId + '/' + this.attribute + '/' + payloadId).set(payload);
    }

    public read(targetId: string, payloadId: string): Observable<any> {
        this.loadingService.set('read-user-' + this.attribute + '-' + payloadId, true);

        return this.afs.doc('users/' + targetId + '/' + this.attribute + '/' + payloadId).valueChanges().pipe(tap(() => {
            this.loadingService.set('read-user-' + this.attribute + '-' + payloadId, false);
        }));
    }

    public delete(targetId: string, payloadId: string): Promise<any> {
        return this.afs.doc('users/' + targetId + '/' + this.attribute + '/' + payloadId).delete();
    }

    public createId(): string {
        return this.afs.createId();
    }
}