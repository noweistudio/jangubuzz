import {Injectable} from '@angular/core';
import {ListAttributeService} from './listing.service';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {FirebaseService} from './firebase.service';

@Injectable({
    providedIn: 'root'
})
export class ListingCommentService extends ListAttributeService {
    protected attribute: string = 'comments';

    public index(id: string): Observable<any> {
        this.loadingService.set('index-listing-' + this.attribute + '-' + id, true);

        return this.afs.collection('listings/' + id + '/' + this.attribute, ref => ref.orderBy('dateCreated', 'desc')).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-listing-' + this.attribute + '-' + id, false);
        }));
    }

    public create(targetId: string, payload: any = {}): Promise<any> {
        return super.create(targetId, this.afs.createId(), payload);
    }
}

export abstract class ListingCommentAttributeService extends FirebaseService {
    protected attribute: string = '';

    public index(eventId: string, commentId: string): Observable<any> {
        this.loadingService.set('index-listing-comment-' + this.attribute + '-' + commentId, true);

        return this.afs.collection('listings/' + eventId + '/comments/' + commentId + '/' + this.attribute).snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-listing-comment-' + this.attribute + '-' + commentId, false);
        }));
    }

    public create(eventId: string, commentId: string, payloadId: string, payload: any = {}): Promise<any> {
        return this.afs.doc('listings/' + eventId + '/comments/' + commentId + '/' + this.attribute + '/' + payloadId).set(payload);
    }

    public delete(eventId: string, commentId: string, attributeId: string): Promise<any> {
        return this.afs.doc('listings/' + eventId + '/comments/' + commentId + '/' + this.attribute + '/' + attributeId).delete();
    }
}
