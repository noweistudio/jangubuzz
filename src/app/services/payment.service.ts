import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs';
import {ApiService} from './api.service';
import {LoadingService} from './loading.service';
import {SnackbarService} from './snackbar.service';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PaymentService {
    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public apiService: ApiService) {
    }

    public purchaseTicket(payload: any, token: string): Observable<any> {
        payload.userId = this.afAuth.auth.currentUser.uid;
        payload.token = token;
        payload.type = 'ticketPurchase';

        this.loadingService.set('purchase-ticket', true);
        return this.apiService.post('purchase_ticket_test', payload).pipe(tap(() => {
            this.loadingService.delete('purchase-ticket');
            this.snackbarService.open('Ticket Purchased.', 'Close');
        }));
    }

    public purchaseFreeTicket(payload: any): Observable<any> {
        payload.userId = this.afAuth.auth.currentUser.uid;
        payload.type = 'ticketPurchase';

        this.loadingService.set('purchase-free-ticket', true);
        return this.apiService.post('free_ticket', payload).pipe(tap(() => {
            this.loadingService.delete('purchase-free-ticket');
            this.snackbarService.open('Ticket Purchased.', 'Close');
        }));
    }

    public requestTicketRefund(payload): Observable<any> {
        payload.userId = this.afAuth.auth.currentUser.uid;
        payload.type = 'ticketRefundRequest';

        this.loadingService.set('request-ticket-refund', true);
        return this.apiService.post('refund_request', payload).pipe(tap(() => {
            this.loadingService.delete('request-ticket-refund');
            this.snackbarService.open('Ticket Refund Requested.', 'Close');
        }));
    }

    public acceptTicketRefundRequest(payload): Observable<any> {
        payload.type = 'ticketRefund';

        this.loadingService.set('accept-ticket-refund-request', true);
        return this.apiService.post('refund_ticket_test', payload).pipe(tap(() => {
            this.loadingService.delete('accept-ticket-refund-request');
            this.snackbarService.open('Ticket Refund Accepted.', 'Close');
        }));
    }

    public declineTicketRefundRequest(payload): Observable<any> {
        payload.type = 'ticketRefundDecline';

        this.loadingService.set('decline-ticket-refund-request', true);
        return this.apiService.post('refund_decline', payload).pipe(tap(() => {
            this.loadingService.delete('decline-ticket-refund-request');
            this.snackbarService.open('Ticket Refund Declined.', 'Close');
        }));
    }
}