import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {AngularFirestore, AngularFirestoreCollection, QueryDocumentSnapshot} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs/index';
import {tap} from 'rxjs/operators';
import {AngularFireAuth} from 'angularfire2/auth';
import {MatDialog} from '@angular/material';
import {SnackbarService} from './snackbar.service';
import {LoadingService} from './loading.service';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class TeamService extends FirebaseService {
    public teamCollection: AngularFirestoreCollection<any>;
    public membersCollection: AngularFirestoreCollection<any>;

    public teamObservable: Observable<any>;

    /* TODO: future function
        public membersObservables: Observable<any>;
    */

    constructor(public loadingService: LoadingService,
                public snackbarService: SnackbarService,
                public afAuth: AngularFireAuth,
                public afs: AngularFirestore,
                public db: AngularFireDatabase,
                public router: Router,
                public dialog: MatDialog,
                public authService: AuthService) {
        super(loadingService, snackbarService, afAuth, afs, db, router, dialog);
    }

    protected init(): void {
        this.teamCollection = this.afs.collection('users/' + this.afAuth.auth.currentUser.uid + '/team');
        this.membersCollection = this.afs.collection('users/' + this.afAuth.auth.currentUser.uid + '/member');

        this.loadingService.set('index-team-members', true);
        /*
            TODO: future function
            this.loadingService.set('index-members', true);
        */

        this.teamObservable = this.teamCollection.snapshotChanges().pipe(tap(() => {
            this.loadingService.set('index-team-members', false);
        }));
        /* TODO: future function
        this.membersObservables = this.membersCollection.valueChanges().pipe(tap(() => {
            this.loadingService.set('index-members', false);
        }));
        */
    }


    public sendInvite(value: QueryDocumentSnapshot<any>): void {
        this.loadingService.set('invite-team-member-' + value.id, true);

        Promise.all([
            this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/memberRequest/' + value.id).set(value.data()),
            this.afs.doc('users/' + value.id + '/teamRequest/' + this.afAuth.auth.currentUser.uid).set(this.authService.userData)
        ]).then(() => {
            this.loadingService.set('invite-team-member-' + value.id, false);
            this.snackbarService.open('Invitation has been sent to ' + value.data().name + '.', 'Close');
        });
    }

    /**
     * TODO: future function
     * @returns {Observable<any>}
     */
    public getInvite(): Observable<any> {
        return this.afs.collection('users/' + this.afAuth.auth.currentUser.uid + '/teamRequest/').valueChanges();
    }


    public removeTeamMember(value: QueryDocumentSnapshot<any>): void {
        this.loadingService.set('delete-team-member' + value.id, true);

        Promise.all([
            this.afs.doc('users/' + this.afAuth.auth.currentUser.uid + '/team/' + value.id).delete(),
            this.afs.doc('users/' + value.id + '/member/' + this.afAuth.auth.currentUser.uid).delete()
        ]).then(() => {
            this.loadingService.set('delete-team-member' + value.id, false);
            this.snackbarService.open(value.data().name + ' has been removed from your team.', 'Close');
        });
    }
}
