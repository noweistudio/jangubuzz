import { TestBed, inject } from '@angular/core/testing';

import { UserNearbyService } from './user-nearby.service';

describe('UserNearbyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserNearbyService]
    });
  });

  it('should be created', inject([UserNearbyService], (service: UserNearbyService) => {
    expect(service).toBeTruthy();
  }));
});
