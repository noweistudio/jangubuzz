import { TestBed, inject } from '@angular/core/testing';

import { UserBuzzSendService } from './user-buzz-send.service';

describe('UserBuzzSendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserBuzzSendService]
    });
  });

  it('should be created', inject([UserBuzzSendService], (service: UserBuzzSendService) => {
    expect(service).toBeTruthy();
  }));
});
