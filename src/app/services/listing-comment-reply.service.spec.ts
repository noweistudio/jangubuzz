import { TestBed, inject } from '@angular/core/testing';

import { ListingCommentReplyService } from './listing-comment-reply.service';

describe('ListingCommentReplyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingCommentReplyService]
    });
  });

  it('should be created', inject([ListingCommentReplyService], (service: ListingCommentReplyService) => {
    expect(service).toBeTruthy();
  }));
});
