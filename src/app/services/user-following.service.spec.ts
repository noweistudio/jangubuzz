import { TestBed, inject } from '@angular/core/testing';

import { UserFollowingService } from './user-following.service';

describe('UserFollowingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserFollowingService]
    });
  });

  it('should be created', inject([UserFollowingService], (service: UserFollowingService) => {
    expect(service).toBeTruthy();
  }));
});
