import { TestBed, inject } from '@angular/core/testing';

import { UserBlockService } from './user-block.service';

describe('UserBlockService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserBlockService]
    });
  });

  it('should be created', inject([UserBlockService], (service: UserBlockService) => {
    expect(service).toBeTruthy();
  }));
});
