import {Component, Input} from '@angular/core';

@Component({
    selector: 'app-text-line-break',
    templateUrl: './text-line-break.component.html',
    styleUrls: ['./text-line-break.component.scss']
})
export class TextLineBreakComponent {
    @Input() text: string = '';
}
