import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextLineBreakComponent } from './text-line-break.component';

describe('TextLineBreakComponent', () => {
  let component: TextLineBreakComponent;
  let fixture: ComponentFixture<TextLineBreakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextLineBreakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextLineBreakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
