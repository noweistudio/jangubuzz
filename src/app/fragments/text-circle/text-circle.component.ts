import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-text-circle',
  templateUrl: './text-circle.component.html',
  styleUrls: ['./text-circle.component.scss']
})
export class TextCircleComponent {
    @Input() text: string = '';
    @Input() textColor: string = '#FFFFFF';
    @Input() background: string = '#000000';
    @Input() circumference: string = '100px';
    @Input() fontSize: string = '20px';
}
