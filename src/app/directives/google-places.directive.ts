import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {} from '@types/googlemaps';

@Directive({
    selector: '[appGooglePlaces]'
})
export class GooglePlacesDirective implements OnInit {
    @Output() onSelect: EventEmitter<any> = new EventEmitter();

    constructor(private elRef: ElementRef) {
    }

    ngOnInit() {
        const autocomplete = new google.maps.places.Autocomplete(this.elRef.nativeElement);
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            this.onSelect.emit(this.getFormattedAddress(autocomplete.getPlace()));
        });
    }

    private getFormattedAddress(place: any): object {
        return {
            formattedAddress: place.formatted_address,
            country: place.address_components[6].short_name,
            longitude: place.geometry.location.lng(),
            latitude: place.geometry.location.lat()
        };
    }
}